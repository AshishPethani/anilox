<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RmaRequestMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        // return $this->view('view.name');
        return $this->markdown('emails.rma-request')
            ->from('contact@zecher.com')
            ->subject('RMA Request!')
            ->to('contact@zecher.com')
            ->with([ 
                'data' => $this->data,
            ]);
    }
}
