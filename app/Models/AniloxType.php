<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AniloxType extends Model
{
    use HasFactory;

    protected $table = 'anilox_type';

    protected $fillable = [
        'id',
        'title',
    ];
}
