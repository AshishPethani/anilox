<?php

namespace App\Models;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class Favourites extends Model

{
    protected $table = 'favourite_products';
    use HasFactory;

    // use SoftDeletes;


    protected $fillable = [

        'id',
        'user_id',
        'product_id',

    ];

    // protected $dates = ['deleted_at'];


}
