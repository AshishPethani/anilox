<?php

namespace App\Models;
// use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class QuotationRequest extends Model

{
    protected $table = 'quotation_request';
    use HasFactory;

    // use SoftDeletes;


    protected $fillable = [
        'id',
        'cms',
        'clean_machine',
        'info_sleeves',
        'email',
        'article',
        'machine_manufacture',
        'machine_type',
        'anilox_type',
        'anilox_surface',
        'order_type',
        'ink_system',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function aniloxSurface()
    {
        return $this->hasOne(AniloxSurface::class, 'id', 'anilox_surface');
    }

    public function aniloxType()
    {
        return $this->hasOne(AniloxType::class, 'id', 'anilox_type');
    }

    public function machineManufacture()
    {
        return $this->hasOne(MachineManufacture::class, 'id', 'machine_manufacture');
    }

    public function orderType()
    {
        return $this->hasOne(OrderType::class, 'id', 'order_type');
    }
     public function product() {
        return $this->hasOne(Product::class, 'article', 'article');
    }
}
