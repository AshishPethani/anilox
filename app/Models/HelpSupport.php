<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HelpSupport extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'help_and_support';

    protected $fillable = [
        'user_id',
        'name',
        'email',
        'subject',
        'feedback',
        // 'volumen',
        // 'gravur',
    ];

    protected $dates = ['deleted_at'];


}
