<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SalesContact extends Model
{
    use HasFactory;

    protected $table = 'sales_contact';

    protected $fillable = [
        'old_sales_contact_id',
        'order_id',
        'company_number',
        'contact_id',
        'created_at',
        'updated_at',
        'geo_lat',
        'geo_long',
        'name',
        'notes',
        'type',
        'partner_id',
        'sales_contact',
        'sales_contact_city',
        'sales_contact_country',
        'sales_contact_email',
        'sales_contact_fax',
        'sales_contact_language',
        'sales_contact_name',
        'sales_contact_phone',
        'sales_contact_province',
        'sales_contact_street',
        'sales_contact_zip',
    ];
}
