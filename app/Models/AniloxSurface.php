<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AniloxSurface extends Model
{
    use HasFactory;

    protected $table = 'anilox_surface';

    protected $fillable = [
        'id',
        'title',
    ];
}
