<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class RMA extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'rma_request';

    protected $fillable = [
        'rma_old_id',
        'customer_address',
        'order_number',
        'serial_number',
        'report_date',
        'error_type',
        'description',
        'customer_refnumber',
        'rma_status',
    ];

    protected $dates = ['deleted_at'];
}