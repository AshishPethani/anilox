<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class FAQs extends Model

{

    use HasFactory;
    use SoftDeletes;

    protected $table = 'faqs';

    protected $fillable = [
        'title_english',
        'description_english',
        'title_german',
        'description_german'
    ];

    protected $dates = ['deleted_at'];


}
