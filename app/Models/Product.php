<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;


use Illuminate\Database\Eloquent\Factories\HasFactory;


use Illuminate\Database\Eloquent\Model;


class Product extends Model

{

    use HasFactory;

    use SoftDeletes;


    protected $fillable = [

        'old_product_id',

        'product',

        'article',

        'description',

        'raster',

        'volume',

        'volumen',

        'angle',

        'engraving_type',

        'gravur',

        'linescreen',

        'dimension',

        'machine_type',

    ];


    protected $dates = ['deleted_at'];


    protected $appends = ["name"];


    public function getNameAttribute()
    {

        return $this->product;

    }

}

