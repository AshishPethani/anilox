<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'id',
        'old_order_id',
        'company_id',
        'partner_id',
        'offer_id',
        'company_id',
        'contact_id',
        'deliveryaddress_name',
        'deliveryaddress_street',
        'deliveryaddress_city',
        'deliveryaddress_zip',
        'deliveryaddress_province',
        'deliveryaddress_country',
        'billingaddress_name',
        'billingaddress_street',
        'billingaddress_city',
        'billingaddress_zip',
        'billingaddress_province',
        'billingaddress_country',
        'customer_contact',
        'order_date',
        'valid_upon',
        'edi',
        'vid_name',
        'vid_link',
        'vad_company',
        'vad_name',
        'price',
        'status',
        'discount',
    ];

    protected $dates = ['deleted_at'];
    protected $appends = ["order_status"];

    public function getOrderStatusAttribute()
    {
        return $this->status;
    }

    public function contact()
    {
        return $this->hasOne(SalesContact::class, 'order_id', 'id');
    }

    public function position()
    {
        return $this->hasMany(OrderPosition::class, 'order_id', 'id');
    }
}
