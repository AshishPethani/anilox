<?php


namespace App\Models;


use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;


class Supplier extends Model

{

    use HasFactory;
    use SoftDeletes;


    protected $fillable = [

        'partner_id',
        'contact_id',
        's_address1',
        'order_id',
        'name',
        'company_number',
        'type',
        'geo_lat',
        'geo_long',

    ];

    protected $dates = ['deleted_at'];


}
