<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PushNotifications extends Model
{
    use HasFactory;

    protected $table = "push_notifications";
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'title',
        'message',
        'is_read',
        'user_id',
        'data',
        'date',
    ];


    protected $casts = [
        'data' => 'array',
        'date' => 'date:Y-m-d'
    ];

}
