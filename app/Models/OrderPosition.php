<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderPosition extends Model
{
    use HasFactory;

    protected $table = 'order_position';

    protected $fillable = [
        'order_id',
        'old_order_position_id',
        'amount',
        'angle',
        'article',
        'created_at',
        'updated_at',
        'customer_serial_number',
        'date_of_delivery',
        'drawing',
        'engraving',
        'linescreen',
        'offer_id',
        'position',
        'product_description',
        'serial_number',
        'status',
        'volume',
    ];
    
    public function rmacount(){
        return $this->hasMany(RMA::class,'serial_number','serial_number');
    }
}
