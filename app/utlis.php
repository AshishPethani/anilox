<?php
  
namespace App;
  
use Edujugon\PushNotification\PushNotification;
use Illuminate\Support\Facades\Storage;
use mysql_xdevapi\Exception;
use Pushok\AuthProvider\Token;
use Pushok\Client;
use Pushok\Payload;
use Pushok\Payload\Alert;
use App\Models\PushNotifications;
use App\Models\NotificationLog;

class utlis
{
   public static function getHostUrl() {
        $PROTOCOL = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $DOMAIN_NAME = $_SERVER['HTTP_HOST'].'/';
        $URL = $PROTOCOL . $DOMAIN_NAME;

        return $URL;
    }

    public static function sendNotificationApn($deviceTokens, $title, $msg, $type, $data)
    {
            $options = [
                'key_id' => '9KLS5HQ8XK',
                'team_id' => 'GWT9RKRF9Z',
                'app_bundle_id' => 'com.MyZecher.app',
                'private_key_path' => Storage::disk('local')->path('AuthKey_CVG56Q2J34.p8'),
                'private_key_secret' => null // Private key secret
            ];

            $authProvider = Token::create($options);

            $alert = Alert::create()->setTitle($title);
            $alert = $alert->setBody($msg);

            $payload = Payload::create()->setAlert($alert);

            $payload->setSound('default');

            $payload->setCustomValue('notification_type', $type);
            $payload->setCustomValue('data', $data);
            
            $notifications = [];
            
          if(is_array($deviceTokens)){
                foreach ($deviceTokens as $item) {
                    $notifications[] = new \Pushok\Notification($payload, $item);
                }
            }else{
                $notifications[] = new \Pushok\Notification($payload, $deviceTokens);
            }

            $client = new Client($authProvider, $production = true);
            $client->addNotifications($notifications);

            $responses = $client->push();
            $respData = [];
            foreach ($responses as $response) {
                // The device token
                $respData[] = $response->getDeviceToken();
                $respData[] = $response->getApnsId();
                $respData[] = $response->getStatusCode();
                $respData[] = $response->getReasonPhrase();
                $respData[] = $response->getErrorReason();
                $respData[] = $response->getErrorDescription();
                $respData[] = $response->get410Timestamp();
            }

            return response()->json($respData);
        
    }

    public static function sendNotificationFcm($tokens, $title, $msg, $type, $data)
    {
        //\Log::info('Hi..');
        $push = new PushNotification('fcm');
        $push->setMessage([
            'notification' => [
                'title' => $title,
                'body' => $msg,
                'sound' => 'default'
            ],
            'data' => [
                'notification_type' => $type,
                'data' => $data,
            ]
        ])
            ->setApiKey(env('SERVER_KEY'))
            ->setDevicesToken($tokens)
            ->send();

        return response()->json($push->getFeedback());
    }

    public static function storeNotification($data, $type=null){
        try {
            $chkAlreadyExt = '';
            if($type="rma_status"){
                $chkAlreadyExt = PushNotifications::where('user_id',$data['user_id'])->where('type',$data['type'])->whereJsonContains('data',['order_number'=>$data['data']['order_number']])->first();
            }       
        
            if(!empty($chkAlreadyExt)){
               PushNotifications::where('user_id',$data['user_id'])->where('type',$data['type'])->whereJsonContains('data',['order_number'=>$data['data']['order_number']])
               ->update([
                   'data'=>$data['data'],
                   'message' => $data['message'],
                   'date' => $data['date']
                   ]);
                   NotificationLog::create($data);
            }
            if(empty($chkAlreadyExt)){
                PushNotifications::create($data);
                 NotificationLog::create($data);
            }
        }
        catch (Exception $e){

        }
    }


}