<?php

namespace App\Exports;
  
use App\Models\RMA;
use Maatwebsite\Excel\Concerns\FromCollection;
  
class RMAExport implements FromCollection
{
   
    public function collection()
    {
        return RMA::all();
    }
}
?>