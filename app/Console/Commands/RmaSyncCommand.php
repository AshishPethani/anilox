<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RMA;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class RmaSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:rma';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively insert data an exclusive quote to everyone daily via cron.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return string
     */
    public function handle()
    {
        dump('sync:rma started on:' . now());
        $response = Http::withToken('SFMyNTY.g2gDYXtuBgAGtDfAewFiAAFRgA.S2Z_xg_HGMYK-1trsDSUX44pJaPGwARb1z4bS03kM3c')
            ->get('http://217.7.121.13:5100/api/rma');

        if ($response->successful()) {
            DB::table('rma_request')->update(['deleted_at' => now()]);

            $rmas = [];
            foreach ($response->object() as $item) {
                $rmas[] = [
                    'rma_old_id' => $item->id,
                    'customer_address' => $item->customer_address,
                    'customer_refnumber' => $item->customer_refnumber,
                    'description' => $item->description,
                    'error_type' => $item->error_type,
                    'order_number' => $item->order_number,
                    'report_date' => $item->report_date,
                    'rma_status' => $item->rma_status,
                    'serial_number' => $item->serial_number,
                    'deleted_at' => null
                ];
            }

            if (!empty($rmas)) {
                foreach (array_chunk($rmas, 1000) as $rma) {
                    DB::table('rma_request')->upsert(
                        $rma,
                        ['rma_old_id'],
                        ['customer_address', 'customer_refnumber', 'description', 'error_type', 'order_number', 'report_date', 'rma_status', 'serial_number', 'updated_at', 'deleted_at']
                    );
                }
            }

            DB::table('rma_request')->whereNotNull('deleted_at')->delete();
            dump('sync:rma finished on:' . now());
        }
        return 'DONE';
    }
}
