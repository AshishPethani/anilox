<?php

namespace App\Console\Commands;

use App\Models\Favourites;
use App\Models\Feedback;
use App\Models\HelpSupport;
use App\Models\NotificationLog;
use App\Models\PushNotifications;
use Illuminate\Console\Command;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class UserSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively insert data an exclusive quote to everyone daily via cron.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return string
     */
    public function handle()
    {
        dump('sync:users started on:' . now());
        $response = Http::withToken('SFMyNTY.g2gDYgAABNJuBgA-jzMQfQFiAAFRgA.MYnPGwfv-R2mtsJi6Vgv5csGf09GmqkjIAu1phHkQQg')
            ->get('http://217.7.121.13:5100/api/users');

        if ($response->successful()) {

            User::where('role', '!=', 'admin')
                ->update(['status' => 0]);

            $users = [];
            foreach ($response->object() as $item) {
                $userExist = User::where('email', $item->email)->first();
                if ($userExist) {
                    DB::table('users')
                        ->where('email', $item->email)
                        ->update([
                            'old_id' => $item->id,
                            'company_id' => $item->company_id,
                            'active' => $item->active,
                            'expiry_date' => $item->expiry_date,
                            'permissions' => (string)$item->permissions,
                            'status' => 1,
                            'created_at' => now(),
                            'updated_at' => now(),
                        ]);
                } else {
                    $users[] = [
                        'old_id' => $item->id,
                        'email' => $item->email,
                        'company_id' => $item->company_id,
                        'active' => $item->active,
                        'expiry_date' => $item->expiry_date,
                        'permissions' => (string)$item->permissions,
                        'status' => 1,
                        'created_at' => now(),
                        'updated_at' => now(),
                    ];
                }
            }

            if (!empty($users)) {
                $usersChunk = array_chunk($users, 1000);
                foreach($usersChunk as $userItem) {
                    DB::table('users')
                        ->insert($userItem);
                }
            }

            $getUser = User::where('role', '!=', 'admin')->where('status', 0)->get('id');
            if (!empty($getUser)) {
                Favourites::whereIn('user_id', $getUser)->forceDelete();
                Feedback::whereIn('user_id', $getUser)->forceDelete();
                HelpSupport::whereIn('user_id', $getUser)->forceDelete();
                NotificationLog::whereIn('user_id', $getUser)->forceDelete();
                PushNotifications::whereIn('user_id', $getUser)->forceDelete();
                User::where('role', '!=', 'admin')->where('status', 0)->forceDelete();
            }
        }
        dump('sync:users finished on:' . now());
        return 'DONE';
    }
}
