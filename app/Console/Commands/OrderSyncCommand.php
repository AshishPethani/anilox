<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class OrderSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively insert data an exclusive quote to everyone daily via cron.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function handle()
    {
        dump('sync:orders started on:' . now());
        $response = Http::withToken('SFMyNTY.g2gDYXtuBgAGtDfAewFiAAFRgA.S2Z_xg_HGMYK-1trsDSUX44pJaPGwARb1z4bS03kM3c')
            ->get('http://217.7.121.13:5100/api/orders');

        if ($response->successful()) {
            DB::table('orders')->truncate();
            DB::table('order_position')->truncate();
            DB::table('sales_contact')->truncate();

            foreach ($response->object() as $key => $item) {
                dump("Processing row $key and old_order_id $item->id");
                $orderId = DB::table('orders')->insertGetId([
                    'old_order_id' => $item->id,
                    'billingaddress_city' => $item->billingaddress_city,
                    'billingaddress_country' => $item->billingaddress_country,
                    'billingaddress_name' => $item->billingaddress_name,
                    'billingaddress_province' => $item->billingaddress_province,
                    'billingaddress_street' => $item->billingaddress_street,
                    'billingaddress_zip' => $item->billingaddress_zip,
                    'deliveryaddress_city' => $item->deliveryaddress_city,
                    'deliveryaddress_country' => $item->deliveryaddress_country,
                    'deliveryaddress_name' => $item->deliveryaddress_name,
                    'deliveryaddress_province' => $item->deliveryaddress_province,
                    'deliveryaddress_street' => $item->deliveryaddress_street,
                    'deliveryaddress_zip' => $item->deliveryaddress_zip,
                    'contact_id' => $item->contact_id,
                    'created_at' => $item->created_at ? Carbon::parse($item->created_at) : now(),
                    'updated_at' => $item->updated_at ? Carbon::parse($item->updated_at) : now(),
                    'customer_contact' => $item->customer_contact,
                    'discount' => $item->discount,
                    'edi' => $item->edi,
                    'offer_id' => $item->offer_id,
                    'order_date' => $item->{'order-date'} ? Carbon::parse($item->{'order-date'}) : null,
                    'partner_id' => $item->partner_id,
                    'price' => $item->price,
                    'status' => $item->status,
                    'vad_company' => $item->vad_company,
                    'valid_upon' => $item->{'valid-upon'} ? Carbon::parse($item->{'valid-upon'}) : null,
                    'vid_link' => $item->vid_link,
                    'vid_name' => $item->vid_name,
                    'vad_name' => $item->vad_name,
                ]);

                if ($item->contact) {
                    DB::table('sales_contact')->insert([
                        'order_id' => $orderId,
                        'old_sales_contact_id' => $item->contact->id,
                        'company_number' => $item->contact->company_number,
                        'contact_id' => $item->contact->contact_id,
                        'created_at' => $item->contact->created_at ? Carbon::parse($item->contact->created_at) : now(),
                        'updated_at' => $item->contact->updated_at ? Carbon::parse($item->contact->updated_at) : now(),
                        'geo_lat' => $item->contact->geo_lat,
                        'geo_long' => $item->contact->geo_long,
                        'name' => $item->contact->name,
                        'notes' => $item->contact->notes,
                        'partner_id' => $item->contact->partner_id,
                        'sales_contact' => $item->contact->{'sales-contact'},
                        'sales_contact_city' => $item->contact->{'sales-contact_city'},
                        'sales_contact_country' => $item->contact->{'sales-contact_country'},
                        'sales_contact_email' => $item->contact->{'sales-contact_email'},
                        'sales_contact_fax' => $item->contact->{'sales-contact_fax'},
                        'sales_contact_language' => $item->contact->{'sales-contact_language'},
                        'sales_contact_name' => $item->contact->{'sales-contact_name'},
                        'sales_contact_phone' => $item->contact->{'sales-contact_phone'},
                        'sales_contact_province' => $item->contact->{'sales-contact_province'},
                        'sales_contact_street' => $item->contact->{'sales-contact_street'},
                        'sales_contact_zip' => $item->contact->{'sales-contact_zip'},
                        'type' => $item->contact->type,
                    ]);
                }

                if ($item->positions) {
                    $OrderPosition = [];
                    foreach ($item->positions as $val) {
                        $OrderPosition[] = [
                            'old_order_position_id' => $val->id,
                            'order_id' => $orderId,
                            'amount' => $val->amount,
                            'angle' => $val->angle,
                            'article' => $val->article,
                            'created_at' => $val->created_at ? Carbon::parse($val->created_at) : now(),
                            'updated_at' => $val->updated_at ? Carbon::parse($val->updated_at) : now(),
                            'customer_serial_number' => $val->{'customer-serial-number'},
                            'date_of_delivery' => $val->date_of_delivery ? Carbon::parse($val->date_of_delivery) : null,
                            'drawing' => $val->drawing,
                            'engraving' => $val->engraving,
                            'linescreen' => $val->linescreen,
                            'offer_id' => $val->offer_id,
                            'position' => $val->position,
                            'product_description' => $val->{'product-description'},
                            'serial_number' => $val->{'serial-number'},
                            'status' => $val->status,
                            'volume' => $val->volume,
                        ];
                    }

                    DB::table('order_position')->insert($OrderPosition,);
                }
            }
        }
        dump('sync:orders finished on:' . now());
    }
}
