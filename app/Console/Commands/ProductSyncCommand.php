<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;

class ProductSyncCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Respectively insert data an exclusive quote to everyone daily via cron.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return JsonResponse
     */
    public function handle()
    {
        dump('sync:products started on:' . now());
        $response = Http::withToken('SFMyNTY.g2gDYgAABNJuBgA-jzMQfQFiAAFRgA.MYnPGwfv-R2mtsJi6Vgv5csGf09GmqkjIAu1phHkQQg')
            ->get('http://217.7.121.13:5100/api/product');

        if ($response->successful()) {
            DB::table('products')->truncate();
            DB::table('favourite_products')->truncate();

            $products = [];
            foreach ($response->object() as $item) {
                $products[] = [
                    'old_product_id' => $item->id,
                    'angle' => $item->angle,
                    'article' => $item->article,
                    'description' => $item->description,
                    'engraving_type' => $item->engraving_type,
                    'linescreen' => $item->linescreen,
                    'product' => $item->product,
                    'volume' => $item->volume,
                    'dimension' => $item->dimension,
                    'gravur' => $item->gravur,
                    'machine_type' => $item->machine_type,
                    'raster' => $item->raster,
                    'volumen' => $item->volumen,
                ];
            }

            if (!empty($products)) {
                foreach (array_chunk($products, 1000) as $product) {
                    DB::table('products')->insert($product);
                }
            }
            dump('sync:products finished on:' . now());
        }
    }
}
