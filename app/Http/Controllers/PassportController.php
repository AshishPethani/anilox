<?php
 
namespace App\Http\Controllers;
 
use App\Models\User;
use Illuminate\Http\Request;
use Validator;
use URL;
use Illuminate\Support\Arr;
 
class PassportController extends Controller
{
    /**
     * Handles Registration Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required|min:3',
        //     'email' => 'required|email|unique:users',
        //     'password' => 'required|min:6',
        // ]);

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            // 'c_password' => 'required|same:password',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }
 
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
 		// print_r($user);exit();
        $token = $user->createToken('appToken')->accessToken;
 		
        return response()->json(['token' => $token], 200);
    }

    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
 
    /**
     * Handles Login Request
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if($validator->fails()){
            $response = array(
                'status' => 0,
                'message' =>'Enter valid email address!',
                'data' => [],
            );
            return json_encode($response);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];
 
        if (auth()->attempt($credentials)) {
            if (Auth()->user()->role == 'accountant') {
                $token = auth()->user()->createToken('Token')->accessToken;
                $data = User::where('id', Auth()->user()->id)->first();
                $results = array(
                    'id' => $data->id,
                    'name' => $data->name,
                    'last_name' => $data->last_name,
                    'username' => $data->username,
                    'm_number' => $data->m_number,
                    'email' => $data->email,
                    'role' => $data->role,
                    'profile_picture' => URL::to('/'.'common_path_for_all_image/'. $data->profile_picture),
                    'token' => $token,
                );

                $response = array(
                    'status' => 1,
                    'message' => 'Accountant User Login Successfully!',
                    'data' => $results,
                );

                return json_encode($response);
                // return response()->json(['token' => $token, 'message' => "Login as Accountant", $data], 200);
            }
            

            if (Auth()->user()->role == 'agent') {
                $token = auth()->user()->createToken('Token')->accessToken;
                $data = User::where('id', Auth()->user()->id)->first();
                $results = array(
                    'id' => $data->id,
                    'name' => $data->name,
                    'last_name' => $data->last_name,
                    'username' => $data->username,
                    'm_number' => $data->m_number,
                    'email' => $data->email,
                    'role' => $data->role,
                    'profile_picture' => URL::to('/'.'common_path_for_all_image/'. $data->profile_picture),
                    'token' => $token,
                );

                $response = array(
                    'status' => 1,
                    'message' => 'Agent User Login Successfully!',
                    'data' => $results,
                );

                return json_encode($response);
                // return response()->json(['token' => $token, 'message' => "Login as Agent"], 200);
            }
            if (Auth()->user()->role == 'worker') {
                $token = auth()->user()->createToken('Token')->accessToken;
                $data = User::where('id', Auth()->user()->id)->first();
                $results = array(
                    'id' => $data->id,
                    'name' => $data->name,
                    'last_name' => $data->last_name,
                    'username' => $data->username,
                    'm_number' => $data->m_number,
                    'email' => $data->email,
                    'role' => $data->role,
                    'profile_picture' => URL::to('/'.'common_path_for_all_image/'. $data->profile_picture),
                    'token' => $token,
                );

                $response = array(
                    'status' => 1,
                    'message' => 'Worker User Login Successfully!',
                    'data' => $results,
                );

                return json_encode($response);
                // return response()->json(['token' => $token, 'message' => "Login as Worker"], 200);
            }

            else
            {
                Auth()->logout();
                $response = array(
                    'status' => 0,
                    'message' => 'user credentials dose not match with our records!',
                    'data' => [],
                );
                return json_encode($response);
            }
            
        } else {
            // return response()->json(['error' => 'UnAuthorised'], 401);
            Auth()->logout();
                $response = array(
                    'status' => 0,
                    'message' => 'user credentials dose not match with our records!',
                    'data' => [],
                );
                return json_encode($response);
        }
    }
 
    /**
     * Returns Authenticated User Details
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function get_profile()
    {
        $data = User::where('id', Auth()->user()->id)->first();
        $results = array(
            'id' => $data->id,
            'name' => $data->name,
            'last_name' => $data->last_name,
            'username' => $data->username,
            'm_number' => $data->m_number,
            'email' => $data->email,
            'role' => $data->role,
            'profile_picture' => URL::to('/'.'common_path_for_all_image/'. $data->profile_picture),
        );
        return $results;
        // return response()->json(['user' => auth()->user()], 200);
    }

    public function get_profile_update(Request $request, $id)
    {
        $input = $request->all();

        // if(!empty($input['password'])){ 
        //     $input['password'] = Hash::make($input['password']);
        // }else{
        //     $input = Arr::except($input,array('password'));    
        // }

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'last_name' => 'required',
            'username' => 'required',
            'm_number' => 'required',
            'email' => 'required|email',
            'profile_picture' => 'nullable|image',
        ]);

        if($validator->fails()){
            return $this->sendError('Validation Error.', $validator->errors());       
        }

        if(!empty($input['profile_picture'])){ 
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image -> move($user_path , $user_name);

            $input['profile_picture'] = ($user_name);
        }else{
            $input = Arr::except($input,array('profile_picture'));    
        }

        $user = User::find(auth()->user()->id);
        $user->update($input);

        return response()->json(['status' => "Successfully updated"], 200);
    }

    public function worker_list(Request $request)
    {
        $data = User::where('role', 'worker')->select('*')->orderBy('id','DESC')->get(); 
        return response()->json(['data' => $data], 200);
    }
}