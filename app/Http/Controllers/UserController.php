<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {

        if ($request->ajax()) { 

             $data = User::select('*')->where('role', '!=', 'admin')->orderBy('id','ASC');
              
             return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){
     
                             $btn = '<a href="'.route("users.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>';
       
                             return $btn;
                     })

                     ->rawColumns(['action'])
                     ->make(true);

            }  
            
        return view('admin.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        request()->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email',
            'username' => 'required',
            'password' => 'required',
            'company_id' => 'required',
            'permissions' => 'required',
        ]);

        $imageName = '';
        if ($request->hasFile('profile_picture')) {
            $user_image = $request->file('profile_picture');
            $imageName = "Profile-" . time() . '.' . $user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image->move($user_path, $imageName);
        }

        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['permissions'] = implode(',', $input['permissions']);
        $input['profile_picture'] = ($imageName);
        User::create($input);

        return redirect()->route('users.index')->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function show($id)
    {
        $user = User::find($id);
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Application|Factory|View|Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email,' . $id,
        ]);
        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        if (!empty($input['profile_picture'])) {
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-" . time() . '.' . $user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image->move($user_path, $user_name);

            $input['profile_picture'] = ($user_name);
        } else {
            $input = Arr::except($input, array('profile_picture'));
        }

        $user = User::find($id);
        $user->update($input);

        return redirect()->back()->with('success', 'Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        User::find($id)->delete();
        return response()->json(['success' => 'User deleted successfully']);
    }

    public function profile()
    {
        $user = User::find(auth()->user()->id);
        return view('admin.users.profile', compact('user'));
    }

    public function profilesql(Request $request)
    {
        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        if (!empty($input['profile_picture'])) {
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-" . time() . '.' . $user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image->move($user_path, $user_name);

            $input['profile_picture'] = ($user_name);
        } else {
            $input = Arr::except($input, array('profile_picture'));
        }

        $user = User::find(auth()->user()->id);
        $user->update($input);

        return redirect()->back()->with('success', 'Updated successfully');
    }

    public function go_back()
    {
        echo '<script type="text/javascript">'
        , 'history.go(-2);'
        , '</script>';
    }
}
