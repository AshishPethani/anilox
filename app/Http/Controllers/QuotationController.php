<?php

namespace App\Http\Controllers;

use App\Models\QuotationRequest;
use App\Models\RMA;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    //     $data = QuotationRequest::with(['aniloxSurface','user','aniloxType','machineManufacture','orderType','product'])->limit(5)->get();
       //  dd($data);
        if ($request->ajax()) {
            $data = QuotationRequest::with(['aniloxSurface','user','aniloxType','machineManufacture','orderType','product']);
            return Datatables::of($data)
                ->addIndexColumn()
                ->editColumn('email', function ($row){
                    return $row->email;
                })
                  ->editColumn('cms', function ($row){
                    return ($row->cms == '0') ? 'No' : 'Yes';
                })
                  ->editColumn('clean_machine', function ($row){
                    return ($row->clean_machine == '0') ? 'No' : 'Yes';
                })
                ->editColumn('info_sleeves', function ($row){
                    return ($row->info_sleeves == '0') ? 'No' : 'Yes';
                })
          /*      ->filter(function ($instance) use ($request) {                  
                   if ($request->input('search.value') != "") {
                         return $instance->where('email','like','%'.$request->input('search.value').'%');
                    }                              
                }) */    
                ->editColumn('created_at', function ($row){
                    return Carbon::parse($row->created_at)->format('d-m-Y H:i:s');
                })
                ->editColumn('anilox_type', function ($row){
                    return !empty($row->aniloxType) ? $row->aniloxType->title : '---';               
                })
                ->editColumn('anilox_surface', function ($row){
                    return !empty($row->aniloxSurface) ? $row->aniloxSurface->title : '---';               
                })
                ->editColumn('anilox_surface', function ($row){
                    return !empty($row->aniloxSurface) ? $row->aniloxSurface->title : '---';               
                })
                 ->editColumn('order_type', function ($row){
                    return !empty($row->orderType) ? $row->orderType->title : '---';               
                })
                ->editColumn('machine_manufacture', function ($row){
                    return !empty($row->machineManufacture) ? $row->machineManufacture->title : '---';               
                })
                ->editColumn('article', function ($row){
                    return !empty($row->article) ? $row->article : '---';               
                })
                 ->editColumn('ink_system', function ($row){
                    return !empty($row->ink_system) ? $row->ink_system : '---';               
                })
                ->editColumn('machine_type', function ($row){
                    return !empty($row->machine_type) ? $row->machine_type : '---';               
                })
               
                ->addColumn('action', function ($row) {

                    $btn = '<a href="' . route("quotation.show", $row->id) . '"><i class="zmdi zmdi-eye" style="color:green;"></i></a>';

                    return $btn;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.quotation.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function show($id)
    {
       $data = QuotationRequest::with('product')->find($id);
        return view('admin.quotation.show', compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        QuotationRequest::find($id)->delete();
        return response()->json(['success' => 'Quotation deleted successfully']);
    }
}
