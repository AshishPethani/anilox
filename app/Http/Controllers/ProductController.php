<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

   if ($request->ajax()) { 

             $data = Product::select('*');

             return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){
     
                             $btn = '<a href="'.route("products.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>';
       
                             return $btn;
                     })

                     ->rawColumns(['action'])
                     ->make(true);

            }  
        return view('admin.products.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // print_r("expression");exit();
        return view('admin.products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'customer_address' => 'required',
            'order_number' => 'required',
            'serial_number' => 'required',
            'report_date' => 'required',
            'error_type' => 'required',
            'description' => 'required',
            'customer_refnumber' => 'required',
        ]);

        Product::create($request->all());
    
        return redirect()->route('admin.products.index')

        ->with('success','RMA created successfully.');
        // echo "<pre>";
        // print_r($request->toArray());exit();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return view('admin.products.show',compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Product::find($id);
        return view('admin.products.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);
        $input = $request->all();

        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

        if(!empty($input['profile_picture'])){ 
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image -> move($user_path , $user_name);

            $input['profile_picture'] = ($user_name);
        }else{
            $input = Arr::except($input,array('profile_picture'));    
        }

        $user = Product::find($id);
        $user->update($input);

        return redirect()->back()->with('success','Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::find($id)->delete();
        return response()->json(['success'=>'User deleted successfully']);
    }


    public function go_back()
    {
       echo '<script type="text/javascript">'
               , 'history.go(-2);'
               , '</script>';
    }
}