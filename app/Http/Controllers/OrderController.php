<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;


class OrderController extends Controller
{
    public function index(Request $request)
    {
         if ($request->ajax()) { 
            $data = Order::select('*'); 
            return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){     
                             $btn = '<a href="'.route("orders.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>';
                             return $btn;
                     })
                     ->rawColumns(['action'])
                     ->make(true);
         }
        return view('admin.orders.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // print_r("expression");exit();
        return view('admin.orders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'company_id'=>'required',
            'partner_id'=>'required',
            'offer_id'=>'required',
            'company_id'=>'required',
            'contact_id'=>'required',
            'deliveryaddress_name'=>'required',
            'deliveryaddress_street'=>'required',
            'deliveryaddress_city'=>'required',
            'deliveryaddress_zip'=>'required',
            'deliveryaddress_province'=>'required',
            'deliveryaddress_country'=>'required',
            'billingaddress_name'=>'required',
            'billingaddress_street'=>'required',
            'billingaddress_city'=>'required',
            'billingaddress_zip'=>'required',
            'billingaddress_province'=>'required',
            'billingaddress_country'=>'required',
            'customer_contact'=>'required',
            'order_date'=>'required',
            'valid_upon'=>'required',
            'edi'=>'required',
            'vid_name'=>'required',
            'vid_link'=>'required',
            'vad_company'=>'required',
            'vad_name'=>'required',
            'price'=>'required',
            'status'=>'required',
            'discount'=>'required',
        ]);

        Order::create($request->all());
    
        return redirect()->route('admin.products.index')->with('success','RMA created successfully.');
        // echo "<pre>";
        // print_r($request->toArray());exit();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $dataArray = Order::find($id);
        return view('admin.orders.show',compact('dataArray'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = Order::find($id);
        return view('admin.orders.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);
        $input = $request->all();

        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

        if(!empty($input['profile_picture'])){ 
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image -> move($user_path , $user_name);

            $input['profile_picture'] = ($user_name);
        }else{
            $input = Arr::except($input,array('profile_picture'));    
        }

        $user = Order::find($id);
        $user->update($input);

        return redirect()->back()->with('success','Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::find($id)->delete();
        return response()->json(['success'=>'User deleted successfully']);
    }


    public function go_back()
    {
       echo '<script type="text/javascript">'
               , 'history.go(-2);'
               , '</script>';
    }
}