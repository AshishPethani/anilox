<?php
namespace App\Http\Controllers;
use App\Models\HelpSupport;
// use App\Models\Supplier;
use DataTables;
use Illuminate\Http\Request;

class HelpSupportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = HelpSupport::all();
        if ($request->ajax()) {
            return Datatables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    return '<a href="' . route("helpSupport.show", $row->id) . '"><i class="zmdi zmdi-eye" style="color:green;"></i></a>&nbsp;
<a data-rowid="' . $row->id . '" class="btn-delete"><i class="zmdi zmdi-delete" style="color:red;"></i></a>&nbsp;';
                })
                ->rawColumns(['action'])
                ->make(true);
        }
        return view('admin.helpSupport.index');

    }

    /**
     * Show the form for creating a new resource.
     
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.helpSupport.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
   */

    public function store(Request $request)

    {

        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'subject'=>'required',
            'feedback'=>'required',
        ]);
        // print_r($request->toArray());exit();
        // $user_image = $request->file('p_image');
        // // print_r($user_image);
        // // exit();
        // $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
        // $user_path = public_path('/common_path_for_all_image');
        // $user_image -> move($user_path , $user_name);   
        // $input = $request->all();
        // // $input['password'] = Hash::make($input['password']);
        // $input['p_image'] = ($user_name);

        HelpSupport::create($request->all());
        // Supplier::create($request->all());

     

        return redirect()->route('admin.helpSupport.index')

                        ->with('success','Product created successfully.');

    }
     

    /**

     * Display the specified resource.

     *
     * @return \Illuminate\Http\Response
     */

    public function show($id)

    {         
        $helpS = HelpSupport::find($id);
        return view('admin.helpSupport.show',compact('helpS'));
    } 

     

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\HelpSupport  $helpS
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */
    public function edit(HelpSupport $helpS)
    {

        return view('admin.helpSupport.edit',compact('helpS'));

    }
    /*
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * 
     * @param  \App\Product  $helpS
     * 
          * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, HelpSupport $helpS)
    {
        $request->validate([
            'name'=>'required',
            'email'=>'required',
            'subject'=>'required',
            'feedback'=>'required',
        ]);

    
        // if(!empty($input['p_image'])){ 
        //     $user_image = $request->file('p_image');
        //     $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
        //     $user_path = public_path('/common_path_for_all_image');
        //     $user_image -> move($user_path , $user_name);

        //     $input['p_image'] = ($user_name);
        // }else{
        //     $input = Arr::except($input,array('p_image'));    
        // }

        $helpS->update($request->all());
        // $supplier->update($request->all());

    

        return redirect()->route('admin.helpSupport.index')

                        ->with('success','Product updated successfully');

    }

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\HelpSupport  $helpS
     * @param  \App\Supplier  $supplier
     * @return \Illuminate\Http\Response
     */

    public function destroy(HelpSupport $helpS )
    {
        $helpS->delete();
        // $supplier->delete();
        return redirect()->route('admin.helpSupport.index')
                        ->with('success','Product deleted successfully');

    }

}