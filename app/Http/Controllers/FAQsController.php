<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\FAQs;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;
use Illuminate\Support\Str;


class FAQsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
            
        if ($request->ajax()) { 


            $data = FAQs::select('*'); 

            return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){
                        
                            $btn = '<a href="'.route("faqs.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>  &nbsp; 
                                    <a href="'.route("faqs.edit",$row->id).'"><i class="zmdi zmdi-edit" style="color:blue;"></i></a>&nbsp;
				   
				                <a href="'.route("faq.delete",$row->id).'"><i class="zmdi zmdi-delete" style="color:red;"></i></a>&nbsp;
                            ';
       
                             return $btn;
                     })
                     
                    ->editColumn('description_english',function($row){
                         
                         return Str::limit($row->description_english, 50, ' (...)');

                     })
                
                     ->rawColumns(['action'])
                     ->make(true);
         }
         
       
        return view('admin.faqs.index');
    }
    
    public function create()
    {
    
        return view('admin.faqs.create');
    }
    
    
    public function store(Request $request)
    {

        $this->validate($request, [
            'title_english' => 'required',
            'description_english' => 'required',
            'title_german' => 'required',
            'description_german' => 'required',
        ]);

        $input = $request->all();

        FAQs::create($input);

        return redirect()->back()->with('success','Created successfully');
    }

     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = FAQs::find($id);
        return view('admin.faqs.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title_english' => 'required',
            'description_english' => 'required',
            'title_german' => 'required',
            'description_german' => 'required',
        ]);

        $input = $request->all();

        $data = FAQs::find($id);
        $data->update($input);

        return redirect()->back()->with('success','Updated successfully');
    }
    
      
    public function delete($id)
    {
    	$data = FAQs::find($id)->delete();
    	return redirect()->back()->with('success','Deleted successfully');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = FAQs::find($id);
        return view('admin.faqs.show',compact('data'));
    }
    
  
}
