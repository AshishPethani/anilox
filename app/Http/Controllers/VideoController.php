<?php

namespace App\Http\Controllers;
use App\Models\Video;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;
use Storage;

class VideoController extends Controller
{
  
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Video::where('status','0')->get();
            return Datatables::of($data)
                     ->addIndexColumn()
                        ->filter(function ($instance) use ($request) {                  
                        if ($request->input('search.value') != "") {
                                return $instance->where('title','like','%'.$request->input('search.value').'%');
                            }                                      
                        })
                        ->editColumn('url', function($row){ 
                            $url = '<a href="'.$row->url.'" ></a>'; 
                            return $url; })
                        ->editColumn('created_at', function ($row){
                            return Carbon::parse($row->created_at)->format('d-m-Y H:i:s');
                        })
                        ->editColumn('video', function ($row){
                            $vids = asset($row->video);
                            return "<video src=' $vids ' height='100' style='min-width: 250px;'  controls> </video>";
                        })
                        ->addColumn('action', function ($row) {
                            $btn = '<a href="' . route('video.show',$row->id) . '"><i class="zmdi zmdi-eye" style="color:green; text-align: center; margin-left: 10px"></i></a>
                                    <a href="' . route('video.edit',$row->id) . '"><i class="zmdi zmdi-edit" style="color:blue; margin-left: 10px "></i></a>
                                    <a href="#" class="remove" data-id="'.$row->id.'"><i class="zmdi zmdi-delete" style="color:red; margin-left: 10px"></i></a>';

                            return $btn;
                        })
                        ->rawColumns(['action','created_at','video'])
                        ->make(true);
        }
        return view('admin.videos.vlist');
    }
  
    public function create()
    {
        return view('admin.videos.uploadVideo');
    }

    public function uploadvideofile()
    {
        return view('admin.videos.fileupload');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'video' => 'required',
        ]);

        if($request->hasFile('video')){
            $video = $request->file('video');
            $video_name = time().'.'.$video->getClientOriginalExtension();
            $video->move(public_path('/videos'),$video_name);        
            $video_path = "/videos/" . $video_name;
        }

        Video::create([
            'title' => $request->title,
            'video' => $video_path,
            'created_at' => now(),
            'updated_at' => null
        ]);
        return redirect()->route('video.index')->with('success', 'video uploaded successfully.');
    }

    public function uploadfile(Request $request)
    {
        if($request->hasFile('video')){
            $video = $request->file('video');
            $video_name = time().'.'.$video->getClientOriginalExtension();
            $video->move(public_path('/upload'),$video_name);
            $video_path = "/upload/" . $video_name;
        }
        return redirect()->back();
    }


    public function show($id)
    {
        $data = Video::find($id);
        return view('admin.videos.showVideo',['data' => $data]);
    }
  
    public function edit($id)
    {
        $data = Video::find($id);
        return view('admin.videos.editVideo',['data' => $data]);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            // 'video' => 'required',
        ]);

        if($request->hasFile('video')){
            $video = $request->file('video');
            $video_name = time().'.'.$video->getClientOriginalExtension();
            $video->move(public_path('/videos'),$video_name);        
            $video_path = "/videos/" . $video_name;
        }
        $data = Video::find($id);
        $data->title = $request->title;
        if($request->video){
            $data->video = $video_path;
        }
        $data->updated_at = now();
        $data->save();
        return redirect()->route('video.index')->with('success', 'Video Updated successfully.');
    }
 
    public function destroy($id)
    {
        $video = Video::find($id);
        $path = $video->video;
        dump($path);
        $video->delete();
        if(Storage::exists($path)){
            // dd($path);
            Storage::delete($path);
        }

        return redirect()->route('video.index')->with('success', 'Video Deleted successfully.');
    }
}
