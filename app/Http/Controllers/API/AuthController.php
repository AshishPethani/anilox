<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Laravue\JsonResponse;
use App\Mail\PasswordGenerated;
use App\Models\User; 
use App\Models\PushNotifications;
use App\Models\Order;
use App\Rules\MatchOldPassword;
use Illuminate\Support\Facades\Hash;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function verifyEmail(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:users,email'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => 0,
                'data' => [],
            ]);
        } else {
            $user = User::where('email', $request->email)->first();

            if (empty($user->username)) {
                $password = Str::random(8);
                $user->password = bcrypt($password);
                $user->tem_password = $password;
                $user->save();

                try {
                    // send email with auto generated password for user
                    Mail::to($user)->send(new PasswordGenerated($user->first_name, $password));
                } catch (\Exception $e) {
                    return response()->json([
                        'message' => $e->getMessage(),
                        'status' => 0,
                        // 'status' => $user->tem_password,
                        'data' => [],
                    ]);
                }

                return response()->json([
                    'message' => 'We have sent you email with your password',
                    'status' => 2,
                    'password' => $user->tem_password,
                    'data' => [],
                ]);
            } else {
                return response()->json([
                    'message' => 'Email successfully Verified',
                    'status' => 1,
                    'password' => $user->tem_password,
                    'data' => [],
                ]);
            }
        }
    }

    public function login(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:users,email',
                'username' => 'required',
                'password' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => 0,
                'data' => [],
            ]);
        } else {
            
            $user = User::where('email', $request->email)->first();

            if (empty($user->username)) {
                $usernameUnique = User::where('username', $request->username)->count();
                if (!empty($usernameUnique)) {
                    return response()->json([
                        'message' => 'Username already exist',
                        'status' => 0,
                        'data' => [],
                    ]);
                }
                dd($user);
                $user->username = $request->username;
                $user->save();
            }

            $credentials = $request->only('username', 'password', 'email');
            if (!Auth::attempt($credentials)) {
                return response()->json([
                    'message' => 'Invalid Credentials',
                    'status' => 0,
                    'data' => [],
                ]);
            }
            $user->device_token = isset($request->device_token) ? $request->device_token : '';
            $user->device_type = isset($request->device_type) ? $request->device_type : '';
            $user->save();

        $accessToken = auth()->user()->createToken('authToken')->accessToken;
                        
            $user->remember_token = $accessToken;
            $user->save();
    
            $availableNotification = PushNotifications::where('user_id', Auth()->user()->id)
                        ->where('is_read', 0)
                        ->count();
                        
            return response()->json([
                'message' => 'Logged in successfully',
                'status' => 1,
                'data' => [
                    'remember_token' => $accessToken,
                    'id' => auth()->user()->id,
                   // 'first_name' => auth()->user()->first_name,
                    'username' => auth()->user()->username,
                    'email' => auth()->user()->email,
                    'device_type' => auth()->user()->device_type,
                    'device_token' => auth()->user()->device_token,
                    'notification_on_off' => auth()->user()->notification_status,
                    'available_notification' => $availableNotification,
                ],
            ]);
        }
    }

    public function forgotPassword(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'email' => 'required|email|exists:users,email'
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => 0,
                'data' => [],
            ]);
        } else {
            try {
                $status = Password::sendResetLink(
                    $request->only('email')
                );

                return response()->json([
                    'message' => __($status),
                    'status' => 1,
                    'data' => [],
                ]);
            } catch (\Exception $e) {
                return response()->json([
                    'message' => $e->getMessage(),
                    'status' => 0,
                    'data' => [],
                ]);
            }
        }
    }
    
 public function changePassword(Request $request)
    { 
        $input = $request->all();
        $validator = Validator::make($request->all(), [
            'current_password' => 'required',new MatchOldPassword,
            'new_password' => 'required',
            'new_confirm_password' => 'required|same:new_password',
        ]);

        if($validator->fails()){
            // return $this->sendError('Validation Error.', $validator->errors()); 
            return response()->json([
                'message' => 'Data Invalid.',
                'status' => 2,
                'data' => [],
            ]);       
        }
        
        if(!\Hash::check($input['current_password'], auth()->user()->password)){
            return response()->json([
                'message' => 'You have entered wrong password.',
                'status' => 0,
                'data' => [],
            ]); 
        }else{
            User::find(auth()->user()->id)->update(['password'=> Hash::make($input['new_password']),'tem_password'=> $input['new_password']]);
            return response()->json([
              'message' => 'Password change successfully.',
              'status' => 1,
              'data' => [],
          ]); 
        
        }
 // dd('Password change successfully.');

    }
    public function sendError($error, $errorMessages = [], $code = 404)
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];


        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }


        return response()->json($response, $code);
    }
   
}