<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Favourites;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FavouriteProductController extends BaseController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add_favourite_product(Request $request)
    {
        $favourite = $request->all();
        $validator = Validator::make(
            $request->all(),
            [
                'user_id' => 'required',
                'product_id' => 'required',
            ]
        );
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => 0,
                'data' => [],
            ]);
        } else {
            $favourite = Favourites::where('product_id', $request->product_id)
                ->where('user_id', $request->user_id)
                ->first();

            if (!empty($favourite)) {
                return response()->json([
                    'message' => 'Already exist in Your favourite',
                    'status' => 2,
                    'data' => [],
                ]);
            }
            $data = new Favourites;
            $data->user_id = $request->input('user_id');
            $data->product_id = $request->input('product_id');
            $data->save();

            return response()->json([
                'status' => 1,
                'message' => 'Added to Your favourite',
                'data' => []
            ]);

        }

    }

    public function favourite_product_list(Request $request)
    {
        $page = 1;
        $perPage = 10;
        $data = Favourites::leftjoin('products AS pd', 'pd.id', '=', 'favourite_products.product_id')
            ->leftjoin('users AS ur', 'ur.id', '=', 'favourite_products.user_id')
            ->select('favourite_products.id', 'favourite_products.user_id', 'favourite_products.product_id', 'ur.username AS username','pd.product', DB::raw('pd.product as name'), 'pd.volume', 'pd.article', 'pd.machine_type', 'pd.linescreen', 'pd.gravur', 'pd.dimension')
            ->where('ur.id', Auth()->user()->id)
            ->orderBy('favourite_products.id', 'DESC')
            ->paginate($perPage)->toArray();
        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Favourite List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);
        return response()->json($data2);

    }

    public function remove_favourite_product(Request $request)
    {
        $validator = Validator::make(
            $request->all(),
            [
                'id' => 'required',
            ]
        );

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->errors()->first(),
                'status' => 0,
                'data' => [],
            ]);
        } else {

            Favourites::where('id', $request->id)->delete();
            return response()->json([
                'status' => 1,
                'message' => 'Removed to Your favourite',
                'data' => []
            ]);
        }
    }
}
