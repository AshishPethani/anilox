<?php

namespace App\Http\Controllers\API;

use App\Mail\QuotationRequestMail;
use App\Mail\RmaRequestMail;
use App\Models\Dimension;
use App\Models\Engraving;
use App\Models\Linescreen;
use App\Models\Year;
use App\Models\MachineType;
use App\Models\OrderPosition;
use App\Models\SalesContact;
use Carbon\Carbon;
use App\utlis;
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Product;
use App\Models\RMA;
use App\Models\Reason;
use App\Models\FAQs;
use App\Models\User;
use App\Models\Order;
use App\Models\CMSPages;
use App\Models\HelpSupport;
use App\Models\QuotationRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Models\PushNotifications;
use App\Models\Announcement;
use App\Models\Video;

class ProductController extends BaseController
{
    public function get_profile()
    {
        $data = User::where('id', Auth()->user()->id)->first();
        
         $data->available_notification = PushNotifications::where('user_id', Auth()->user()->id)
                        ->where('is_read', 0)
                        ->count();
        
        return response()->json([
            'status' => 1,
            'message' => 'Profile Data',
            'data' => $data
        ]);
    }

    public function helpSupport()
    {
        $data = HelpSupport::get();
        return response()->json([
            'status' => 1,
            'message' => 'Help & Support Data',
            'data' => $data
        ]);
    }

    public function get_product(Request $request)
    {
        $perPage = 10;
        $article = $request->article;
        $machine_type = $request->machine_type;
        $linescreen = $request->linescreen;
        $gravur = $request->gravur;
        $dimension = $request->dimension;

        $data = Product::where(function ($q) use ($article) {
            if (empty($article)) {
                $q->where('article', 'NOT LIKE', "340%")
                    ->where('article', 'NOT LIKE', "370%")
                    ->where('article', 'NOT LIKE', "DZ%");
            } else {
                $q->where('article', 'LIKE', "$article%");
            }
        })
         ->where(function ($q) use ($machine_type) {
               if (!empty($machine_type)) {
            $q->where('machine_type', 'LIKE', "%$machine_type%");
               }
        })
        ->where(function ($q) use ($linescreen) {
               if (!empty($linescreen)) {
            $q->where('linescreen', 'LIKE', "%$linescreen%");
               }
        })
        ->where(function ($q) use ($gravur) {
              if (!empty($gravur)) {
                $q->where('gravur', '=', "$gravur");
                $q->orWhere('engraving_type', '=', "$gravur");
              }
        })
     
        ->where(function ($q) use ($dimension) {
             if (!empty($dimension)) {
            $q->where('dimension', 'LIKE', "%$dimension%");
             }
        })
        ->paginate($perPage)->toArray();


        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Product List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);
    }

    public function search_product(Request $request)
    {

        $perPage = 10;
        $name = $request->title;
        $article = $request->article;
        
        $data = Product::where(function ($q) use ($name) {

            $q->where('product', 'LIKE', "%" . $name . "%")
                ->orWhere('article', 'LIKE', "%" . $name . "%")
                ->orWhere('volume', 'LIKE', "%" . $name . "%");

        })->where(function ($q) use ($article) {
                      if (empty($article)) {
                          $q->where('article', 'NOT LIKE', "%340%")
                              ->where('article', 'NOT LIKE', "%370%")
                              ->where('article', 'NOT LIKE', "%DZ%");
                      } else {
                          $q->where('article', 'LIKE', "%$article%");
                      }
                  })

            ->paginate($perPage)
            ->toArray();

        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Your Product here';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);
    }

    public function get_product_by_id(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'product_id' => 'required|exists:products,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'msg' => $validator->errors()->first(),
                'data' => [],
            ]);
        }

        $data = Product::where('id', $request->product_id)->first();

        return response()->json([
            'status' => 1,
            'msg' => 'Product detail',
            'data' => [
                'id' => $data->id,
                'old_product_id' => $data->old_product_id,
                'name' => $data->name,
                'article' => $data->article,
                'description' => $data->description,
                'engraving_type' => $data->engraving_type,
                'linescreen' => $data->linescreen,
                'volume' => $data->volume,
                'angle' => $data->angle,
                'raster' => $data->raster,
                'volumen' => $data->volumen,
                'gravur' => $data->engraving_type,
                'dimension' => $data->dimension,
                'machine_type' => $data->machine_type,
                'created_at' => $data->created_at,
                'updated_at' => $data->updated_at,
                'deleted_at' => $data->deleted_at,
            ],
        ]);
    }


    public function get_rma_data()
    {
        $perPage = 10;

        $data = DB::table('rma_request as rma')
            ->join('orders as ord', 'rma.order_number', '=', 'ord.old_order_id')
            ->join('sales_contact as sc', 'sc.order_id', '=', 'ord.id')
            ->select('rma.*')
            ->where('sc.company_number', Auth::user()->company_id)
            ->orderBy('rma.id', 'DESC')
            ->paginate($perPage)
            ->toArray();

        $data3 = [];
        foreach ($data['data'] as $key => $item) {
            $data3[$key] = (array)$item;
            $data3[$key]['image'] = '';
        }

        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'RMA List';
        $data2['data'] = $data3;
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);
    }

    public function machine_manufacture()
    {
        $result = DB::select('SELECT id, title FROM `machine_manufacture`');
        return response()->json([
            'status' => 1,
            'message' => 'Machine Manufactures',
            'data' => $result,
        ]);
    }

    public function linescreen()
    {
        $result = Linescreen::get();
        return response()->json([
            'status' => 1,
            'message' => 'Linescreen',
            'data' => $result,
        ]);
    }

    public function engraving()
    {
        $result = Engraving::get();
        return response()->json([
            'status' => 1,
            'message' => 'Engraving',
            'data' => $result,
        ]);
    }

    public function dimension()
    {
        $result = Dimension::get();
        return response()->json([
            'status' => 1,
            'message' => 'Dimension',
            'data' => $result,
        ]);
    }

    public function machine_type()
    {
        $result = MachineType::get();
        return response()->json([
            'status' => 1,
            'message' => 'Machine Types',
            'data' => $result,
        ]);
    }

    public function anilox_type()
    {
        $result = DB::select('SELECT id, title FROM `anilox_type`');
        return response()->json([
            'status' => 1,
            'message' => 'Anilox Type',
            'data' => $result,
        ]);
    }

    public function anilox_surface()
    {
        $result = DB::select('SELECT id, title FROM `anilox_surface`');
        return response()->json([
            'status' => 1,
            'message' => 'Anilox Surface',
            'data' => $result,
        ]);
    }

    public function order_type()
    {
        $result = DB::select('SELECT id, title FROM `order_type`');
        return response()->json([
            'status' => 1,
            'message' => 'Order Types',
            'data' => $result,
        ]);
    }

    public function quotation_request(Request $request)
    {
       
        if (empty($request->article)) {
            $validator = Validator::make(
                $request->all(),
                [
                    'machine_manufacture' => 'required',
                    'machine_type' => 'required',
                    'anilox_type' => 'required',
                    'anilox_surface' => 'required',
                    'order_type' => 'required',
                    'ink_system' => 'required',
                    'cms' => 'required',
                    'clean_machine' => 'required',
                    'info_sleeves' => 'required'
                   // 'email' =>'required|email',
                ]
            );
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                    'status' => 0,
                    'data' => [],
                ]);
            } else {
                $quotation_req = new QuotationRequest;
                $quotation_req->email = $request->email;
                $quotation_req->machine_manufacture = $request->input('machine_manufacture');
                $quotation_req->machine_type = $request->input('machine_type');
                $quotation_req->anilox_type = $request->input('anilox_type');
                $quotation_req->anilox_surface = $request->input('anilox_surface');
                $quotation_req->order_type = $request->input('order_type');
                $quotation_req->ink_system = $request->input('ink_system');
                $quotation_req->cms = $request->cms;
                $quotation_req->clean_machine = $request->clean_machine;
                $quotation_req->info_sleeves = $request->info_sleeves;
                $quotation_req->save();
                try {
                    Mail::to($request->email)->send(new QuotationRequestMail($request->email, $quotation_req));
                } catch (\Exception $e) {
                }

                return response()->json([
                    'status' => 1,
                    'message' => 'Quotation request sent Successfully.',
                    'data' => [],
                ]);
            }
        } else {
            $validator = Validator::make(
                $request->all(),
                [
                    'article' => 'required',
                    'cms' => 'required',
                    'clean_machine' => 'required',
                    'info_sleeves' => 'required'
                ]
            );
            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->errors()->first(),
                    'status' => 0,
                    'data' => [],
                ]);
            } else {
                $checkExist = QuotationRequest::where('article',$request->article)->where('email',$request->email)
                    ->first();
                if(!empty($checkExist)){
                    return response()->json([
                        'status' => 0,
                        'message' => 'Quotation request already exist.',
                        'data' => [],
                    ]);

                }
                $quotation_req = new QuotationRequest;
                $quotation_req->email = $request->email;
                $quotation_req->article = $request->input('article');
                $quotation_req->cms = $request->input('cms');
                $quotation_req->clean_machine = $request->input('clean_machine');
                $quotation_req->info_sleeves = $request->input('info_sleeves');
                $quotation_req->save();

                try {
                    $data = QuotationRequest::with('product')->find($quotation_req->id);
                    Mail::to($request->email)->send(new QuotationRequestMail($request->email, $data));
                } catch (\Exception $e) {
                    \Log::info($e->getMessage());
                }

                return response()->json([
                    'status' => 1,
                    'message' => 'Quotation request sent Successfully.',
                    'data' => [],
                ]);
            }
        }
    }

    public function get_orders(Request $request)
    {
        //dd($request->order_status);

        $perPage = 10;
        $orderStatus = explode(",",$request->order_status);
        $status = array();
        if(in_array('4',$orderStatus)){
            $status[] = "Finished";
            $status[] = "order finished";
        }
        if(in_array('1',$orderStatus)){
            $status[] = "Order received";
            $status[] = "in preparation";
        }
        if(in_array('2',$orderStatus)){
            $status[] = "In production";
        }
        if(in_array('3',$orderStatus)){
            $status[] = "Shipped";
        }
        
        $data = Order::select('orders.*', 'rma_request.id as RMA_order_id')
            ->join('sales_contact as sc', 'sc.order_id', '=', 'orders.id')
            ->leftJoin('rma_request', 'orders.old_order_id', '=', 'rma_request.order_number')
            ->where('sc.company_number', Auth::user()->company_id)
            ->where(function ($q) use ($request) {
                if (!empty($request->order_number)) {
                    $q->where('orders.old_order_id', 'LIKE', $request->order_number);
                }
            })
            ->where(function ($q) use ($request) {
                if (!empty($request->year)) {
                    $q->whereYear('order_date', $request->year);
                }
            })
            ->where(function ($q) use ($request, $status) {
                if (!empty($request->order_status)) {
                    $q->whereIn('status', $status);
                }
            })
            ->groupBy('orders.id')
            ->paginate($perPage)
            ->toArray();

       
            $data2 = [];
            $data2['status'] = 200;
            $data2['message'] = 'Order List';
            $data2['data'] = $data['data'];
            $data2['total'] = $data['total'];
            $data2['per_page'] = $data['per_page'];
            $data2['total_page'] = $data['last_page'];
            $data2['current_page'] = $data['current_page'];
            $data2['total_record_per_page'] = count($data['data']);
            
        foreach ($data2['data'] as $key => $value) {
            $data2['data'][$key]['Position'] = OrderPosition::withCount('rmacount as rmacount')->where('order_id', $value['id'])->get();
            $data2['data'][$key]['Contact'] = SalesContact::where('order_id', $value['id'])->first();
        }

        return response()->json($data2);
    }

    public function order_details($id)
    {
        $data = Order::where('orders.id', '=', $id)
            ->select('orders.*', 'rma_request.id as RMA_order_id')
            ->LeftJoin('rma_request', 'orders.old_order_id', 'rma_request.order_number')
            ->orderBy('orders.id', 'DESC')
            ->first();

        $data2 = [];
        $data2['status'] = 200;
        $data2['message'] = 'Order List';
        $data2['data'] = $data;
        $data2['data']['Position'] = OrderPosition::withCount('rmacount as rmacount')->where('order_id', $id)->get();
        $data2['data']['Contact'] = SalesContact::where('order_id', $data->id)->first();

        return response()->json($data2);
    }

    public function reason()
    {
        $perPage = 10;
        $data = Reason::select('id', 'title')->paginate($perPage)->toArray();

        $data2 = [];
        $data2['status'] = 200;
        $data2['message'] = 'Reason List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);
    }

    public function rma_request(Request $request)
    {
        $input = $request->all();
        $order = "";
        if (!empty($request->order_id)) {
            $validator = Validator::make($input, [
                'serial_number' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }

            $count = DB::table('rma_request as rma')
                ->join('orders as ord', 'rma.order_number', '=', 'ord.old_order_id')
                ->join('sales_contact as sc', 'sc.order_id', '=', 'ord.id')
                ->where('sc.company_number', Auth::user()->company_id)
                ->where('rma.serial_number', $request->serial_number)
                ->count();

            if ($count) {
                return response()->json([
                    'status' => 0,
                    'message' => 'RMA already exists.',
                    'data' => [],
                ]);
            }

            // $orderPosition = DB::table('orders as ord')
            //     ->where('ord.old_order_id', $request->order_id)
            //     ->join('order_position as op', 'op.order_id', '=', 'ord.id')
            //     ->whereNotNull('op.serial_number')
            //     ->where('op.serial_number', '!=', '')
            //     ->select('op.*')
            //     ->first();
        } else {
            $validator = Validator::make($input, [
                'serial_number' => 'required',
            ]);

            if ($validator->fails()) {
                return $this->sendError('Validation Error.', $validator->errors());
            }
            $order = DB::table('orders as ord')
                ->join('sales_contact as sc', 'sc.order_id', '=', 'ord.id')
                ->join('order_position as op', 'op.order_id', '=', 'ord.id')
                ->where('sc.company_number', Auth::user()->company_id)
                ->where('op.serial_number', $request->serial_number)
                ->first();

            if (empty($order)) {
                return response()->json([
                    'status' => 0,
                    'message' => 'Order not found for scanned product.',
                    'data' => [],
                ]);
            }

            $count = DB::table('rma_request as rma')
                ->join('orders as ord', 'rma.order_number', '=', 'ord.old_order_id')
                ->join('sales_contact as sc', 'sc.order_id', '=', 'ord.id')
                ->where('sc.company_number', Auth::user()->company_id)
                ->where('rma.order_number', $order->old_order_id)
                ->count();

            if ($count) {
                return response()->json([
                    'status' => 0,
                    'message' => 'RMA already exists.',
                    'data' => [],
                ]);
            }
        }

        $rma = new RMA();
        $rma->order_number = isset($request->order_id) ? $request->order_id : $order->old_order_id;
        $rma->error_type = $request->reason;
        $rma->description = $request->description;
        $rma->customer_address = $request->customer_address;
        $rma->customer_refnumber = $request->customer_refnumber;

        // if (!empty($request->order_id) && !empty($orderPosition)) {
        //     $rma->serial_number = $orderPosition->serial_number;
        // }

        if (!empty($request->serial_number)) {
            $rma->serial_number = $request->serial_number;
        }

        $rma->rma_status = 'In Progress';
        $rma->report_date = Carbon::now();
        $rma->save();
        
        try {
            Mail::send(new RmaRequestMail(['data' => $rma]));            
        } catch (\Exception $e) {
            \Log::info($e->getMessage());
        }

        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Successfull';

        return response()->json($data2);
    }

    public function rma_details($id)
    {
        $data = RMA::where('id', '=', $id)
            ->first();


        $data2 = [];
        $data2['status'] = 200;
        $data2['message'] = 'Order List';
        $data2['data'] = $data;

        return response()->json($data2);
    }

    public function faqs()
    {
        $perPage = 10;
        $data = FAQs::paginate($perPage)->toArray();

        $data2 = [];
        $data2['status'] = 200;
        $data2['message'] = 'FAQs List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);

    }

    public function web_pages($page)
    {
        $perPage = 10;
        $data = CMSPages::find($page);

        if (empty($data)) {
            return response('CMS Page Not Found');
        }

        return response($data->content);
    }

    public function notification()
    {
        $perPage = 10;
        $data = PushNotifications::where('user_id', Auth::user()->id)->orderBy('id', 'DESC')->paginate($perPage)->toArray();
        $data2 = [];
        if (!empty($data)) {

            $data2['status'] = 1;
            $data2['message'] = 'Notification List';
            $data2['data'] = $data['data'];
            $data2['total'] = $data['total'];
            $data2['per_page'] = $data['per_page'];
            $data2['total_page'] = $data['last_page'];
            $data2['current_page'] = $data['current_page'];
            $data2['total_record_per_page'] = count($data['data']);
        } else {
            $data2['status'] = 1;
            $data2['message'] = 'Notification List';
            $data2['data'] = [];
            $data2['total'] = 0;
            $data2['per_page'] = $perPage;
            $data2['total_page'] = 1;
            $data2['current_page'] = 1;
            $data2['total_record_per_page'] = 0;
        }
        return response()->json($data2);
    }

    public function order_details_serial_number($serial_number)
    {
        $data = Order::select('orders.*', 'rma_request.id as RMA_order_id')
            ->LeftJoin('rma_request', 'orders.old_order_id', 'rma_request.order_number')
            ->join('order_position as op', 'orders.id', 'op.order_id')
            ->where('op.serial_number', $serial_number)
            ->orderBy('orders.id', 'DESC')
            ->first();
        if (empty($data)) {
            return response()->json([
                'status' => 0,
                'message' => 'Searial number not found.',
                'data' => [],
            ]);
        }

        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Order List';
        $data2['data'] = $data;

        $data2['data']['Position'] = OrderPosition::where('order_id', $data->id)->get();
        $data2['data']['Contact'] = SalesContact::where('order_id', $data->id)->first();

        return response()->json($data2);
    }

    public function help_and_support(Request $request)
    {
        $input = $request->all();
        $rules = [
            'email' => 'required|email',
            'name' => 'required|string|max:30',
            'subject' => 'required|string|max:50',
            'feedback' => 'required|string|max:200',
        ];

        $validator = Validator::make($input, $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => $validator->errors()->first(),
                'data' => []
            ]);
        } else {
            $helpSupport = new HelpSupport();
            $helpSupport->user_id = Auth::user()->id;
            $helpSupport->name = $input['name'];
            $helpSupport->email = $input['email'];
            $helpSupport->subject = $input['subject'];
            $helpSupport->feedback = $input['feedback'];
            $helpSupport->save();

            return response()->json([
                'status' => 1,
                'message' => 'Feedback send successfully.',
                'data' => $helpSupport,
            ]);
        }
    }

    public function is_notify($status)
    {
        User::where('id', Auth::user()->id)->update([
            'notification_status' => $status
        ]);
        if ($status == 1) {
            return response()->json([
                'status' => 1,
                'message' => 'Notification enabled successfully.',
                'data' => [],
            ]);
        } else {
            return response()->json([
                'status' => 1,
                'message' => 'Notification disabled successfully.',
                'data' => [],
            ]);
        }
    }

    public function sendNotification(Request $request)
    {
        $checkUser = User::where('id', Auth::user()->id)->where('notification_status', 1)->first();
        if (isset($checkUser)) {
            if ($request['devicetype'] == 'ios') {
                $check = utlis::sendNotificationApn($request['devicetoken'], 'test notification', 'test notifcation',
                    'test', 'test');
                dd($check);
            }
            if ($request['devicetype'] === 'android') {

                $check = utlis::sendNotificationFcm([$request['devicetoken']], 'isha test notfication', 'isha test notfication',
                    'Hiii', 'isha test notfication');
                dd($check);
            }
        } else {
            return response()->json([
                'status' => 0,
                'message' => 'Notification was disabled by you.',
                'data' => [],
            ]);

        }
    }
    
      public function readNotification(Request $request)
    {
       $readnotification = PushNotifications::where('user_id',Auth::user()->id)->update(['is_read' => 1]);
      if($readnotification){
        return response()->json([
            'status' => 1,
            'message' => 'Notification read Successfully.',
            'data' => [],
        ]);
      }else{
        return response()->json([
            'status' => 0,
            'message' => 'Notification not read Successfully.',
            'data' => [],
        ]);
      }
    }

    public function quotation_list()
    {
        $perPage = 10;
             $data = QuotationRequest::with('aniloxSurface:id,title')
                ->with('aniloxType:id,title')
                ->with('machineManufacture:id,title')
                ->with('orderType:id,title')
                ->with('product')
                ->orderBy('id','DESC')
                ->where('email', Auth::user()->email)
                ->paginate($perPage)
                ->toArray();
     
        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Quotation List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);

        return response()->json($data2);
    }
    public function announcement_list(){
        $perPage = 1000;
        $data = Announcement::where('status','0')->paginate($perPage)
        ->toArray();
        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Announcement List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);
        return response()->json($data2);
    }
    public function video_list(){
        $perPage = 10;
        $data = Video::where('status','0')->paginate($perPage)
        ->toArray();
        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Video List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);
        return response()->json($data2);
    }
    public function year_list(){
        $perPage = 100;
        $data = Year::select('year')->paginate($perPage)
        ->toArray();
        $data2 = [];
        $data2['status'] = 1;
        $data2['message'] = 'Year List';
        $data2['data'] = $data['data'];
        $data2['total'] = $data['total'];
        $data2['per_page'] = $data['per_page'];
        $data2['total_page'] = $data['last_page'];
        $data2['current_page'] = $data['current_page'];
        $data2['total_record_per_page'] = count($data['data']);
        return response()->json($data2);
    }
}
