<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Order;
use App\Models\Product;
use App\Models\RMA;
use App\Models\User;
use Illuminate\Http\Request;
use stdClass;

class PassportClientController extends BaseController
{
    public function getUsers()
    {
        $data = User::where('status','1')->get()
            ->map(function ($data) {
                $user = new stdClass();;
                $user->active = $data->active;
                $user->company_id = $data->company_id;
                $user->email = $data->email;
                $user->expiry_date = $data->expiry_date;
                $user->permissions = $data->permissions;
                $user->id = $data->old_id;
                $user->status = $data->status;
                return $user;
            });

        return response()->json($data);
    }

    public function getRma()
    {
        $data = RMA::get()
            ->map(function ($data) {
                $user = new stdClass();;
                $user->customer_address = $data->customer_address;
                $user->customer_refnumber = $data->customer_refnumber;
                $user->description = $data->description;
                $user->error_type = $data->error_type;
                $user->id = $data->id;
                $user->order_number = $data->order_number;
                $user->report_date = $data->report_date;
                $user->rma_status = $data->rma_status;
                $user->serial_number = $data->serial_number;
                return $user;
            });

        return response()->json($data);
    }

    public function getProducts(){
        $data = Product::get()
            ->map(function ($data) {
                $user = new stdClass();;
                $user->angle = $data->angle;
                $user->article = $data->article;
                $user->description = $data->description;
                $user->engraving_type = $data->engraving_type;
                $user->id = $data->id;
                $user->linescreen = $data->linescreen;
                $user->product = $data->product;
                $user->volume = $data->volume;
               $user->machine_type = $data->machine_type;
               $user->linescreen = $data->linescreen;
               $user->gravur = $data->gravur;
               $user->dimension = $data->dimension;
                return $user;
            });

        return response()->json($data);
    }

    public function getOrders(){
        $data = Order::with('contact')
            ->with('position')
            ->get()
            ->map(function ($data) {
                $user = new stdClass();;
                $user->billingaddress_city = $data->billingaddress_city;
                $user->billingaddress_country = $data->billingaddress_country;
                $user->billingaddress_name = $data->billingaddress_name;
                $user->billingaddress_province = $data->billingaddress_province;
                $user->billingaddress_street = $data->billingaddress_street;
                $user->billingaddress_zip = $data->billingaddress_zip;
                $user->contact_id = $data->contact_id;
                $user->created_at = $data->created_at;
                $user->customer_contact = $data->customer_contact;
                $user->deliveryaddress_city = $data->deliveryaddress_city;
                $user->deliveryaddress_country = $data->deliveryaddress_country;
                $user->deliveryaddress_name = $data->deliveryaddress_name;
                $user->deliveryaddress_province = $data->deliveryaddress_province;
                $user->deliveryaddress_street = $data->deliveryaddress_street;
                $user->deliveryaddress_zip = $data->deliveryaddress_zip;
                $user->discount = $data->discount;
                $user->edi = $data->edi;
                $user->id = $data->id;
                $user->offer_id = $data->offer_id;
                $user->{'order-date'} = $data->order_date;
                $user->partner_id = $data->partner_id;
                $user->price = $data->price;
                $user->status = $data->status;
                $user->updated_at = $data->updated_at;
                $user->vad_company = $data->vad_company;
                $user->vad_name = $data->vad_name;
                $user->{'valid-upon'} = $data->valid_upon;
                $user->vid_link = $data->vid_link;
                $user->vid_name = $data->vid_name;

                //contact
                $user->contact['company_number'] = $data->contact->company_number ?? '';
                $user->contact['contact_id'] = $data->contact->contact_id ?? '';
                $user->contact['created_at'] = $data->contact->created_at ?? '';
                $user->contact['geo_lat'] = $data->contact->geo_lat ?? '';
                $user->contact['geo_long'] = $data->contact->geo_long ?? '';
                $user->contact['id'] = $data->contact->old_sales_contact_id ?? '';
                $user->contact['name'] = $data->contact->name ?? '';
                $user->contact['notes'] = $data->contact->notes ?? '';
                $user->contact['partner_id'] = $data->contact->partner_id ?? '';
                $user->contact['sales-contact'] = $data->contact->sales_contact ?? '';
                $user->contact['sales-contact_city'] = $data->contact->sales_contact_city ?? '';
                $user->contact['sales-contact_country'] = $data->contact->sales_contact_country ?? '';
                $user->contact['sales-contact_fax'] = $data->contact->sales_contact_fax ?? '';
                $user->contact['sales-contact_language'] = $data->contact->sales_contact_language ?? '';
                $user->contact['sales-contact_name'] = $data->contact->sales_contact_name ?? '';
                $user->contact['sales-contact_phone'] = $data->contact->sales_contact_phone ?? '';
                $user->contact['sales-contact_province'] = $data->contact->sales_contact_province ?? '';
                $user->contact['sales-contact_street'] = $data->contact->sales_contact_street ?? '';
                $user->contact['sales-contact_zip'] = $data->contact->sales_contact_zip ?? '';
                $user->contact['type'] = $data->contact->type ?? '';
                $user->contact['updated_at'] = $data->contact->updated_at ?? '';

                foreach ($data->position as $key => $item){
                    $user->position[$key]['amount'] = $item->amount;
                    $user->position[$key]['angle'] = $item->angle;
                    $user->position[$key]['article'] = $item->article;
                    $user->position[$key]['created_at'] = $item->created_at;
                    $user->position[$key]['customer-serial-number'] = $item->customer_serial_number;
                    $user->position[$key]['date_of_delivery'] = $item->date_of_delivery;
                    $user->position[$key]['drawing'] = $item->drawing;
                    $user->position[$key]['engraving'] = $item->engraving;
                    $user->position[$key]['id'] = $item->old_order_position_id;
                    $user->position[$key]['linescreen'] = $item->linescreen;
                    $user->position[$key]['offer_id'] = $item->offer_id;
                    $user->position[$key]['position'] = $item->position;
                    $user->position[$key]['product-description'] = $item->product_description;
                    $user->position[$key]['serial-number'] = $item->serial_number;
                    $user->position[$key]['status'] = $item->status;
                    $user->position[$key]['updated_at'] = $item->updated_at;
                    $user->position[$key]['volume'] = $item->volume;
                }
                return $user;
            });

        return response()->json($data);
    }
}


