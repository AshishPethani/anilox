<?php

namespace App\Http\Controllers;

use App\Models\NotificationLog;
use App\utlis;
use Illuminate\Http\Request;
use App\Models\PushNotifications;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class NotificationController extends Controller
{
    public function index()
    {
        $getuserlist = User::where('id', Auth::user()->id)->where('notification_status', 1)->first();
    }

    public function create()
    {
        return view('admin.notification.create');
    }

    public static function storeNotification(Request $request)
    {
        $users = User::where('status', 1)->get();
        $data = [];
        $iosData = [];
        $androidData = [];

        foreach ($users as $item) {
            $data[] = [
                'user_id' => $item->id,
                'data' => $request->title,  
                'title' => $request->title,
                'message' => $request->description,
                'type' =>"admin_notification",
            ];

            if ($item->device_type == 'ios') {
                $iosData[] = $item->device_token;
            }

            if ($item->device_type == 'android') {
                $androidData[] = $item->device_token;
            }
        }

        PushNotifications::insert($data);

        foreach (array_chunk($iosData, 1000) as $val) {
            utlis::sendNotificationApn($val, $request->title, $request->description,
                'admin', []);
        }

        foreach (array_chunk($androidData, 1000) as $val) {
            utlis::sendNotificationFcm($val, $request->title, $request->description,
                'admin', []);
        }

        return redirect()->route('notification.create')->with('success', 'Notification was send successfully.');

        // return response()->json([
        //     'data'=>[
        //         'ios'=>$iosData,
        //         'android'=>$androidData,
        //     ],
        //     'status' => 0,
        //     'message' => 'Notification was send successfully',
        // ]);
    }
}
