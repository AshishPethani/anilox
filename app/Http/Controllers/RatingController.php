<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Rating;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;


class RatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

      if ($request->ajax()) { 

            $data = Rating::select('rating.id','users.first_name','products.product','rating.rating')
            ->LeftJoin('users','rating.user_id','users.id')
            ->Leftjoin('products','rating.product_id','products.id')
            ->orderBy('rating.id','DESC');
         
            return Datatables::of($data)
                     ->addIndexColumn()
                     ->rawColumns(['action'])
                     ->make(true);
         }

      
      
        return view('admin.rating.index');
    }

 
   
}