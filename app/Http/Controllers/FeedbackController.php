<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Feedback;
use App\Models\User;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;


class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        if ($request->ajax()) { 


            $data = Feedback::select('users.first_name','feedback.id','feedback.feedback')
                ->LeftJoin('users','feedback.user_id','users.id'); 

            return Datatables::of($data)
                     ->addIndexColumn()
		     ->addColumn('action', function($row){
                        
                            $btn = '<a href="'.route("feedback.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>  &nbsp; 
                             
                            ';
       
                             return $btn;
                     })
                     ->rawColumns(['action'])
                     ->make(true);
         }
        
        
        return view('admin.feedback.index');
    }

 	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Feedback::select('users.first_name','feedback.id','feedback.feedback')
                ->LeftJoin('users','feedback.user_id','users.id')
		->where('feedback.id',$id)
                ->first();

        return view('admin.feedback.show',compact('data'));
    }
   
}