<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Product;
use App\Models\Order;
use App\Models\RMA;

class DashboardController extends Controller
{
    public function dashboard()
    {
        $usersCount = User::select('id')->where('role', 'user')->count();
        $productCount = Product::select('id')->count();
        $orderCount = Order::select('id')->count();
        $RMACount = RMA::select('id')->count();
        return view('admin.dashboard', compact('usersCount', 'productCount', 'orderCount', 'RMACount'));
    }

}
