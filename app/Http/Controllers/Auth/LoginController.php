<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth; 

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;



    protected function authenticated($request,$user) { 


                // print_r($request->toArray());exit();
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);


        // $remember_me = $request->has('remember_me') ? true : false; 

        $remember_me = $request->input('remember_me');

        // print_r($remember_me);exit();


        if (auth()->attempt(['email' => $request->input('email'), 'password' => $request->input('password')], $remember_me))
        {
            $user = auth()->user();
            if ($user->role === 'admin') {
                // print_r('done');exit();
                return redirect('/dashboard');
             } else {
                // print_r("qqq");exit();
                Auth::logout();
                return redirect('/login');
                
             }
        }else{
            return back()->with('error','your username and password are wrong.');
        }
     
}
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
