<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CMSPages;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;


class CMSPagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
 
        if ($request->ajax()) { 


            $data = CMSPages::select('*'); 

            return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){
     
                            $btn = '<a href="'.route("cms.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>  &nbsp; 
                                <a href="'.route("cms.edit",$row->id).'"><i class="zmdi zmdi-edit" style="color:blue;"></i></a>&nbsp;
                                
                                 <a href="'.route("cms_page.delete",$row->id).'"><i class="zmdi zmdi-delete" style="color:red;"></i></a>&nbsp;
                            ';
       
                             return $btn;
                     })

                     ->rawColumns(['action'])
                     ->make(true);
         }
       
        return view('admin.cms.index');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // print_r("expression");exit();
        return view('admin.cms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'title' => 'required',
            'content' => 'required',
        ]);

        $input = $request->all();
       
        CMSPages::create($input);
    
        return redirect()->route('cms.index')->with('success','CMS Page created successfully.');
        // echo "<pre>";
        // print_r($request->toArray());exit();
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = CMSPages::find($id);
        return view('admin.cms.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
        ]);


        $cms = CMSPages::find($id);
        $cms->title = $request->input('title');
        $cms->content = $request->input('content');
	$cms->save();
	
        return redirect()->back()->with('success','Updated successfully');
    }
    
    public function delete($id)
    {
    	$data = CMSPages::find($id)->delete();
    	return redirect()->back()->with('success','Deleted successfully');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = CMSPages::find($id);
        return view('admin.cms.show',compact('data'));
    }



   
}
