<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\RMA;
use DB;
use Hash;
use Illuminate\Support\Arr;
use DataTables;
use App\utlis;
use Illuminate\Support\Facades\Auth;
use mysql_xdevapi\Exception;


class RMAController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

         if ($request->ajax()) { 


            $data = RMA::select('*'); 

            return Datatables::of($data)
                     ->addIndexColumn()
                     ->addColumn('action', function($row){
     
                            $btn = '<a href="'.route("rmas.show",$row->id).'"><i class="zmdi zmdi-eye" style="color:green;"></i></a>';
       
                             return $btn;
                     })

                     ->rawColumns(['action'])
                     ->make(true);
         }

      
        
        return view('admin.rmas.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // print_r("expression");exit();
        return view('admin.rmas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'customer_address' => 'required',
            'order_number' => 'required',
            'serial_number' => 'required',
            'report_date' => 'required',
            'error_type' => 'required',
            'description' => 'required',
            'customer_refnumber' => 'required',
        ]);

        RMA::create($request->all());
    
        return redirect()->route('admin.rmas.index')

        ->with('success','RMA created successfully.');
        // echo "<pre>";
        // print_r($request->toArray());exit();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rma = RMA::find($id);
        $reportDate = date('Y-m-d',strtotime($rma->report_date));
        return view('admin.rmas.show',compact('rma','reportDate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admin.rmas.edit',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
        ]);
        $input = $request->all();

        if(!empty($input['password'])){ 
            $input['password'] = Hash::make($input['password']);
        }else{
            $input = Arr::except($input,array('password'));    
        }

        if(!empty($input['profile_picture'])){ 
            $user_image = $request->file('profile_picture');
            $user_name = "Profile-".time().'.'.$user_image->getClientOriginalExtension();
            $user_path = public_path('/common_path_for_all_image');
            $user_image -> move($user_path , $user_name);

            $input['profile_picture'] = ($user_name);
        }else{
            $input = Arr::except($input,array('profile_picture'));    
        }

        $user = RMA::find($id);
        $user->update($input);

        return redirect()->back()->with('success','Updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RMA::find($id)->delete();
        return response()->json(['success'=>'User deleted successfully']);
    }


    public function go_back()
    {
       echo '<script type="text/javascript">'
               , 'history.go(-2);'
               , '</script>';
    }
    
    public function edit_status(Request $request)
    {
        $id = $request->id;
        $data = RMA::find($id);
        $data->rma_status = $request->status;
        $data->save();
        
         $getUsers = DB::table('rma_request as rma')
            ->select('u.*')
            ->join('orders as ord', 'rma.order_number', '=', 'ord.old_order_id')
            ->join('sales_contact as sc', 'sc.order_id', '=', 'ord.id')
            ->join('users as u','u.company_id','sc.company_number')
            ->where('rma.order_number', $data['order_number'])
            ->get();


        try {
           foreach ($getUsers as $key=>$value){

                $rmaType = 'rma_status';
                $message = 'Your RMA status updated to '.$request->status;
                $subject = "RMA status updated";
                $rmaTitle = "RMA Status";
                

               $storeData = [
                   'type' =>$rmaType,
                   'message' => $message,
                   'data' => $data,
                   'user_id' => $value->id,
                   'date' => date('Y-m-d')
               ];
               utlis::storeNotification($storeData,$rmaType);
               if($value->notification_status == 1){
                   if(isset($value->device_type)){
                       if($value->device_type == "ios"){
                           $check = utlis::sendNotificationApn($value->device_token,$rmaTitle,$message,$rmaType,$storeData);
                            
                       }
                       if($value->device_type == "android"){
                           utlis::sendNotificationFcm($value->device_token,$rmaTitle,$message,$rmaType,$storeData);
                       }
                   }
               }
           }
        }
        catch (Exception $e){

        }

        return response()->json([
            'status' => 200,
            'message' => 'Updated successfully',
        ]);
    }
}