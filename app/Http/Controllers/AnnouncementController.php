<?php

namespace App\Http\Controllers;
use App\Models\Announcement;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Carbon\Carbon;


class AnnouncementController extends Controller
{
    public function index(Request $request)
    {
        
        if ($request->ajax()) {
            $data = Announcement::where('status','0')->get();
            return Datatables::of($data)
            ->addIndexColumn()
            ->filter(function ($instance) use ($request) {                  
                   if ($request->input('search.value') != "") {
                         return $instance->where('title','like','%'.$request->input('search.value').'%');
                        }                                      
                    })
                    ->editColumn('created_at', function ($row){
                        return Carbon::parse($row->created_at)->format('d-m-Y H:i:s');
                    })
                    ->editColumn('url', function ($row){
                      return "<a href='$row->url' target='_blank'/> $row->url </a>";
                     })
                    ->editColumn('image', function ($row){
                          $pics = asset($row->image);
                        return "<img src=' $pics ' max-width='150' height='80' />";
                    })
               
                    ->addColumn('action', function ($row) {
                        $btn = '<a href="' . route('announcement.show',$row->id) . '" ><i class="zmdi zmdi-eye" style="color:green; margin-left:10px; "></i></a>
                                <a href="' . route('announcement.edit',$row->id) . '" ><i class="zmdi zmdi-edit" style="color:blue;  margin-left:10px;"></i></a>
                                <a href="#" class="remove" data-id="'.$row->id.'"><i class="zmdi zmdi-delete" style="color:red;  margin-left:10px;"></i></a>';
                            return $btn;
                    })
                    ->rawColumns(['action','created_at','image','url'])
                    ->make(true);
        }
        return view('admin.announcement.index');
    }

    public function create()
    {
        return view('admin.announcement.create');
    }

    public function store(Request $request)
    {
       $request->validate([
            'title' => 'required',
            'description' =>'required',
            'url' => 'required|url',
        ]);
        
        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $image->move(public_path('/images'),$image_name);        
            $image_path = "/images/" . $image_name;
        }else{
            $request->validate([
                    'image' => 'required'
                ]);
        }

        Announcement::create([
            'title' => $request->title,
            'description' =>$request->description,
            'url' => $request->url,
            'image' => $image_path,
            'created_at' => now(),
            'updated_at' => null
        ]);
       
        return redirect()->route('announcement.index')->with('success', 'Announcement created successfully.');
    
    }

    public function show($id)
    {
        $data = Announcement::find($id);
        return view("admin.announcement.show",['data' => $data]);
    }

    public function edit($id)
    {
        $data = Announcement::find($id);
        return view("admin.announcement.edit",['data' => $data]);
    }

    public function updatedata(Request $request,Announcement $announcement)
    {
        $request->validate([
            'title' => 'required',
            'description' =>'required',
            'url' => 'required|url',
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_name = $image->getClientOriginalName();
            $image->move(public_path('/images'),$image_name);        
            $image_path = "/images/" . $image_name;
        }

        $announcement->title = $request->title;
        $announcement->url = $request->url;
        $announcement->description = $request->description;
        if($request->image){
            $announcement->image = $image_path;
        }else{
            
        }
        $announcement->save();

        return redirect()->route('announcement.index')->with('success', 'Announcement Updated successfully.');
    }

    public function delete_announcement($id)
    {
        Announcement::where('id',$id)->delete();
        // Alert::warning('Warning Title', 'Warning Message');
        return redirect()->route('announcement.index')->with('success', 'Announcement Deleted successfully.');

    }
}
