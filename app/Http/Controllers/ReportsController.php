<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Product;
use App\Models\RMA;
use App\Exports\OrderExport;
use App\Exports\ProductExport;
use App\Exports\RMAExport;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\Datatables\Datatables;


class ReportsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     * @throws Exception
     */
    public function orders(Request $request)
    {
        if ($request->ajax()) {
            $data = Order::select('*');

            return Datatables::of($data)
                ->addIndexColumn()
                ->make(true);
        }

        return view('admin.reports.orders');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     * @throws Exception
     */
    public function products(Request $request)
    {
        if ($request->ajax()) {

            $data = Product::select('*');

            return Datatables::of($data)
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.reports.products');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     * @throws Exception
     */
    public function rmas(Request $request)
    {
        if ($request->ajax()) {
            $data = RMA::select('*');

            return Datatables::of($data)
                ->addIndexColumn()
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.reports.rmas');
    }

    public function orderexport()
    {
        return Excel::download(new OrderExport, 'orders.xlsx');
    }

    public function productexport()
    {
        return Excel::download(new ProductExport, 'products.xlsx');
    }

    public function rmaexport()
    {
        return Excel::download(new RMAExport, 'rma.xlsx');
    }
}