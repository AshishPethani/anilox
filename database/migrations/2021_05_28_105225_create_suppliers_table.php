<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->id('s_id');
            $table->string('s_name');
            $table->string('s_address');
            $table->string('s_address1')->nullable();
            $table->string('s_pincode');
            $table->string('s_city');
            $table->string('s_state');
            $table->string('s_contact');
            $table->string('s_contact1')->nullable();
            $table->string('s_whatsapp')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suppliers');
    }
}
