<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_details', function (Blueprint $table) {
            $table->id('s_id');
            $table->string('s_name');
            $table->string('s_address');
            $table->string('s_address1');
            $table->string('s_pincode');
            $table->string('s_city');
            // $table->string('s_district');
            $table->string('s_state');
            $table->string('s_contact');
            $table->string('s_contact1');
            $table->string('s_whatsapp');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_details');
    }
}
