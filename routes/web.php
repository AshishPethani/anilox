<?php

use App\Http\Controllers\QuotationController;
use App\Http\Controllers\AnnouncementController;
use App\Http\Controllers\VideoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\RMAController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\HelpSupportController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
})->name('login');
// Auth::routes();

Route::get('/success', function () {
    return view('auth.success');
})->name('success');

Auth::routes(['register' => false]);

Route::group(['middleware' => ['auth']], function () {

    Route::get('/dashboard', function () {
        return view('admin.dashboard');
    })->name('dashboard');

    Route::get('/go_back', [App\Http\Controllers\UserController::class, 'go_back'])->name('go_back');
    // Route::get('/go_back_product', [App\Http\Controllers\ProductController::class, 'go_back_product'])->name('go_back_product');

    Route::get('/profile', [App\Http\Controllers\UserController::class, 'profile'])->name('profile');
    // Route::get('/p_profile', [App\Http\Controllers\ProductController::class, 'p_profile'])->name('p_profile');

    Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'dashboard'])->name('dashboard');

    Route::post('/profilesql', [App\Http\Controllers\UserController::class, 'profilesql'])->name('profilesql');
    // Route::post('/p_profilesql', [App\Http\Controllers\ProductController::class, 'p_profilesql'])->name('p_profilesql');

    Route::get('forget-password', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showForgetPasswordForm'])->name('forget.password.get');


    Route::post('forget-password', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'submitForgetPasswordForm'])->name('forget.password.post');
    Route::get('reset-password/{token}', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'showResetPasswordForm'])->name('reset.password.get');
    Route::post('reset-password-post', [App\Http\Controllers\Auth\ForgotPasswordController::class, 'submitResetPasswordForm'])->name('reset.password.post');

    Route::get('/delete_user/{id}', [UserController::class, 'destroy']);
    Route::get('/delete_user/{id}', [UserController::class, 'destroy']);

    Route::get('change-password', [App\Http\Controllers\ChangePasswordController::class, 'index']);
    Route::post('change-password', [App\Http\Controllers\ChangePasswordController::class, 'store'])->name('change.password');


    Route::resource('users', UserController::class);
    Route::resource('products', ProductController::class);
    Route::resource('rmas', RMAController::class);
    Route::resource('quotation', QuotationController::class);
    
    
    Route::resource('announcement', AnnouncementController::class);
    Route::post('updatedata/{announcement}', [AnnouncementController::class, 'updatedata'])->name('updatedata');
    Route::get('delete_announcement/{id}', [AnnouncementController::class, 'delete_announcement'])->name('delete_announcement');

    Route::get('video/index', [VideoController::class, 'index'])->name('video.index');
    Route::get('video/create', [VideoController::class, 'create'])->name('video.create');
    Route::get('video/uploadvideofile', [VideoController::class, 'uploadvideofile'])->name('video.uploadvideofile');
    Route::post('video/store', [VideoController::class, 'store'])->name('video.store');
    Route::post('video/uploadfile', [VideoController::class, 'uploadfile'])->name('video.uploadfile');
    Route::get('video/edit/{id}', [VideoController::class, 'edit'])->name('video.edit');
    Route::post('video/update/{id}', [VideoController::class, 'update'])->name('video.update');
    Route::get('video/delete/{id}', [VideoController::class, 'destroy'])->name('video.delete');
    Route::get('video/show/{id}', [VideoController::class, 'show'])->name('video.show');
    
    
    Route::resource('orders', OrderController::class);
    Route::resource('changePassword', ChangePasswordController::class);
    Route::resource('helpSupport', HelpSupportController::class);

    Route::resource('rating', App\Http\Controllers\RatingController::class);
	Route::resource('faqs', App\Http\Controllers\FAQsController::class);
	Route::resource('feedback', App\Http\Controllers\FeedbackController::class);
	Route::resource('cms', App\Http\Controllers\CMSPagesController::class);

	Route::get('reports/orders','App\Http\Controllers\ReportsController@orders')->name('reports.orders');
	Route::get('reports/products','App\Http\Controllers\ReportsController@products')->name('reports.products');
	Route::get('reports/rmas','App\Http\Controllers\ReportsController@rmas')->name('reports.rmas');

	Route::post('rmas/edit_status','App\Http\Controllers\RMAController@edit_status')->name('rmas.rmas');
    Route::get('faq/delete/{id}','App\Http\Controllers\FAQsController@delete')->name('faq.delete');
    Route::get('cms_page/delete/{id}','App\Http\Controllers\CMSPagesController@delete')->name('cms_page.delete');
  
  /* Notification */
    Route::get('user_notification','App\Http\Controllers\NotificationController@index')->name('notification.index');
    Route::get('create-notification','App\Http\Controllers\NotificationController@create')->name('notification.create');
    Route::post('store-notification','App\Http\Controllers\NotificationController@storeNotification')->name('notification.storeNotification');
    /*End Notification*/  
    
    Route::get('order/export','App\Http\Controllers\ReportsController@orderexport')->name('order.export');
    Route::get('product/export','App\Http\Controllers\ReportsController@productexport')->name('product.export');
    Route::get('rma/export','App\Http\Controllers\ReportsController@rmaexport')->name('rma.export');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
