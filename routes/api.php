<?php

use Illuminate\Http\Request;
use App\Http\Controllers\API\PassportClientController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PassportController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\FavouriteProductController;
use App\Http\Controllers\API\ProductController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'force_json_response'], function () {
    
    Route::group(['middleware' => 'passport_client', 'prefix' => 'oauth'], function () {
        /** call POST /oauth/token api to get token
         * Params
         * - grant_type = client_credentials
         * - client_id = ?
         * - client_secret = ?
         **/

        Route::get('users', [PassportClientController::class,'getUsers']);
        Route::get('rma', [PassportClientController::class,'getRma']);
        Route::get('products', [PassportClientController::class,'getProducts']);
        Route::get('orders', [PassportClientController::class,'getOrders']);
    });
    Route::post('send-notification',[ProductController::class, 'sendNotification'])->name('send.notification');

    Route::get('verify-email', [AuthController::class, 'verifyEmail']);
    Route::post('login', [AuthController::class, 'login']);
    Route::post('forgot-password', [AuthController::class, 'forgotPassword']);

    Route::get('get_product', [ProductController::class, 'get_product'])->name('get_product');
    Route::get('get_product_by_id', [ProductController::class, 'get_product_by_id'])->name('get_product_by_id');
    Route::get('search_product', [ProductController::class, 'search_product'])->name('search_product');
    Route::get('faqs', [ProductController::class, 'faqs'])->name('faqs');
    Route::get('web_pages/{page}', [ProductController::class, 'web_pages'])->name('web_pages');


  Route::get('machine_type', [ProductController::class, 'machine_type'])->name('machine_type');
        Route::get('linescreen', [ProductController::class, 'linescreen'])->name('linescreen');
        Route::get('engraving', [ProductController::class, 'engraving'])->name('engraving');
        Route::get('dimension', [ProductController::class, 'dimension'])->name('dimension');


        Route::get('anilox_type', [ProductController::class, 'anilox_type'])->name('anilox_type');
        Route::get('quotation_request', [ProductController::class, 'quotation_request'])->name('quotation_request');
         Route::get('machine_manufacture', [ProductController::class, 'machine_manufacture'])->name('machine_manufacture');
      
        Route::get('anilox_surface', [ProductController::class, 'anilox_surface'])->name('anilox_surface');
        Route::get('order_type', [ProductController::class, 'order_type'])->name('order_type');



    Route::middleware('auth:api')->group(function () {
        Route::post('change-password', [AuthController::class, 'changePassword']);
        Route::get('help-and-support', [ProductController::class, 'helpSupport']);

        Route::get('order_by_serial_number/{serial_number}', [ProductController::class, 'order_details_serial_number'])
            ->name('order_details_serial_number');
        Route::post('help_and_support', [ProductController::class, 'help_and_support'])->name('help_and_support');

        Route::put('is_notify/{status}', [ProductController::class, 'is_notify'])->name('is_notify');

        // Profile
        Route::get('get_profile', [ProductController::class, 'get_profile'])->name('get_profile');
        Route::get('get_orders', [ProductController::class, 'get_orders'])->name('get_orders');
        Route::get('order_details/{id}', [ProductController::class, 'order_details'])->name('order_details');

        Route::get('rma_details/{id}', [ProductController::class, 'rma_details'])->name('rma_details');
        Route::get('reason', [ProductController::class, 'reason'])->name('reason');
        Route::post('rma_request', [ProductController::class, 'rma_request'])->name('rma_request');
        Route::get('favourite_product_list', [FavouriteProductController::class, 'favourite_product_list'])->name('favourite_product_list');
        Route::get('add_favourite_product', [FavouriteProductController::class, 'add_favourite_product'])->name('add_favourite_product');
        Route::get('remove_favourite_product', [FavouriteProductController::class, 'remove_favourite_product'])->name('remove_favourite_product');


        Route::get('get_rma_data', [ProductController::class, 'get_rma_data'])->name('get_rma_data');
        Route::get('push_notification', [ProductController::class, 'notification'])->name('notification');
        Route::post('send_notification', [ProductController::class, 'sendNotification'])->name('send_notification');
        
         Route::get('quotation_list', [ProductController::class, 'quotation_list'])->name('quotation_list');
         Route::post('read_notification', [ProductController::class, 'readNotification'])->name('readNotification');
    });
    
        Route::get('announcement_list', [ProductController::class, 'announcement_list'])->name('announcement_list');
        Route::get('video_list', [ProductController::class, 'video_list'])->name('video_list');
        Route::get('year_list', [ProductController::class, 'year_list'])->name('year_list');
});
