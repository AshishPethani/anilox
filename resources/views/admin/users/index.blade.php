@extends('admin.layout')

@section('content')
<?php
$i = 1;
?>
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>User List
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">User List</li>
                </ul>
            </div>
        </div>
    </div>
   <!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                     @if ($message = Session::get('success'))
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold">Well done!</span> {{$message}}.
                              </div>
                      @endif
                    <div class="header">
                        <ul class="header-dropdown m-r--5">
                            <a href="{{route('users.create')}}" class="btn btn-raised btn-primary m-t-15 waves-effect">Add User</a>
                        </ul><br>
                    </div>
                
                    
                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover js-basic-example " id="user_table">
                            <thead style="background-color:#ededed;">
                                <tr class="text-center">
                                    <th>ID</th>
                                    <th>FIRST NAME</th>
                                    <th>LAST NAME</th>
                                    <th>USERNAME</th>
                                    <th>EMAIL</th>
                                    <th>PERMISSION</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                           
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>  

    <div class="modal" id="DeleteArticleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">User Delete</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h7>Are you sure want to delete this User?</h7>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeleteArticleForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            
             var table = $('#user_table').DataTable({
                   ajax: "{{ route('users.index') }}",
                   columns: [
                       {data: 'id', name: 'id'},
                       {data: 'first_name', name: 'first_name'},
                       {data: 'last_name', name: 'last_name'},
                       {data: 'username', name: 'username'},
                       {data: 'email', name: 'email'},
                       {data: 'permissions', name: 'permissions'},
                       {data: 'action', name: 'action', orderable: false, searchable: false},
                   ]
               });
                var deleteID;
                $('body').on('click', '#getDeleteId', function () {
                    deleteID = $(this).data('id');
                })
                $('#SubmitDeleteArticleForm').click(function (e) {
                    e.preventDefault();
                    var id = deleteID;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "delete_user/" + id,
                        method: 'GET',
                        success: function (result) {
                            // $('#DeleteArticleModal').hide();
                            // $('#DeleteArticleModal').modal('hide');
                            
                            window.location.href = "delete_success_message_user";
                            location.reload();
                            // $(".alert").hide();
                            // $(".notify_alert").click();
                        }
                    });
                });


        });
    </script>
    
@endsection     