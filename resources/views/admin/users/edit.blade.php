@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Edit User</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Edit User</li>
                </ul>
            </div>
        </div>
    </div>
 
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                     @if ($message = Session::get('success'))
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold">Well done!</span> {{$message}}.
                              </div>
                      @endif
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown">
                            
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="{{ route('users.update',$user->id) }}" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            @csrf
                           @method('PUT')
                           <div class="row">
                                <div class="col-md-6">
                                    <label for="name">First Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter your First Name" value="{{$user->first_name}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Last Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter your First Name" value="{{$user->last_name}}">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email">Email</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <!-- <input type="email" id="email" name="email" class="form-control" placeholder="Enter your Email Address" value="{{$user->email}}"> -->
                                            <input type="email" id="email" name="email" class="form-control" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" placeholder="Enter your Email Address" value="{{$user->email}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Username </label>
                                    <small class="text-danger">(read only)</small>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="username" name="username" class="form-control" placeholder="Enter your Username" value="{{$user->username}}"readonly="">
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="password">Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="password" name="password" class="form-control" placeholder="Make it blank if you dont want to change">
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <label for="company_id">Company ID </label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="number" id="company_id" name="company_id" class="form-control" placeholder="Enter Company ID"autocomplete="off"value="{{$user->company_id}}">                      
                                        </div>
                                    </div>
                                </div>
</div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Profile Picture</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            
                                            <input id="uploadImage" type="file" name="profile_picture"  class="form-control hidden" onchange="PreviewImage();" />
                                            <label for="uploadImage" class="btn btn-secondary">Click Me To Change Image</label>                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <img src="{{ url('/') }}/common_path_for_all_image/{{ $user->profile_picture }}" id="uploadPreview" style="width: 100px; height: 70px;" />
                                      </div>
                                </div>    
                            </div>
                            <div class="row">
                          
                                <div class="col-md-6 mt-4">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-primary waves-effect">Save</button>
                                      </div>
                                </div> 
                            </div>
                           
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };

</script>

<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
</script> 

<script type="text/javascript">
      $( "#myform" ).validate({

            last_name: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            email: {
              required: true,
              email: true
            },
            username: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            m_number: {
              required: true,
              minlength: 10,
              maxlength: 10
            },
            password: {
                minlength: 8,
                maxlength: 16
            },
            // profile_picture: {
            //   required: true
            // },
            
          },
          messages: {
           
            last_name: {
              required: "Please provide a last name",
            },
            email: {
              required: "Please provide a email",
              email: "Please provide a valid email"
            },
            username: {
              required: "Please provide a username",
            },
            m_number: {
              required: "Please provide a mobile number",
            },
            password: {
              minlength: "Your password must be at least 8 characters long"
            },
            // profile_picture: {
            //   required: "Please provide a profile picture",
            // },
            
          }
        });
    </script>
    
@endsection     