@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View User</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">View User</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <a href="{{route("users.edit",$user->id)}}"><i class="zmdi zmdi-edit"></i></a>
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                           
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">First Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter your First Name" value="{{$user->first_name}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Last Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter your First Name" value="{{$user->last_name}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">User Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="username" name="username" class="form-control" placeholder="Enter your Username" value="{{$user->username}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email">Email</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <!-- <input type="email" id="email" name="email" class="form-control" placeholder="Enter your Email Address" value="{{$user->email}}"> -->
                                            <input type="email" id="email" name="email" class="form-control" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" placeholder="Enter your Email Address" value="{{$user->email}}" readonly="">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="company_id">Company ID </label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="number" id="company_id" name="company_id" class="form-control" placeholder="Enter Company ID"autocomplete="off" readonly value="{{$user->company_id}}">                      
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <label for="permissions">Permissions</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="text" id="permissions" name="permissions" class="form-control" readonly placeholder="Select Permissions ID"autocomplete="off"value="{{$user->permissions}}">                      
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Profile Picture</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            
                                        <img src="{{ url('/') }}/common_path_for_all_image/{{ $user->profile_picture }}" id="uploadPreview" style="width: 100px; height: 70px;" />

                                        </div>
                                    </div>
                                </div>
                                   
                            </div>
                         
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    

@endsection     