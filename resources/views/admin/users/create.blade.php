@extends('admin.layout')
<style>
.select2-container--default .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: #888 transparent transparent transparent;
    border-style: solid;
    border-width: 5px 4px 0 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}

.select2-container--open .select2-selection--multiple:before {
    content: ' ';
    display: block;
    position: absolute;
    border-color: transparent transparent #888 transparent;
    border-width: 0 4px 5px 4px;
    height: 0;
    right: 6px;
    margin-left: -4px;
    margin-top: -2px;top: 50%;
    width: 0;cursor: pointer
}
  </style>
@section('content')
    <div class="block-header">
    <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>New User</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Add User</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="{{ route('users.store') }}" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">First Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="first_name" name="first_name" class="form-control" placeholder="Enter Your First Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Last Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="last_name" name="last_name" class="form-control" placeholder="Enter Your Last Name">
                                        
                                          </div>
                                    </div>
                                </div>  
                            </div>                           
                            <div class="row">                          
                                <div class="col-md-6">
                                    <label for="email">Email</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="email" id="email" name="email" class="form-control" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" placeholder="Enter Your Email Address"autocomplete="off">
                                            <!-- <input type="email" name="email" id="email" pattern="[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\.[a-zA-Z]{2,4}" style="text-transform: lowercase" placeholder="enter email here ..." title="please enter a valid email" /> -->
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <label for="name">User Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="username" name="username" class="form-control" placeholder="Enter Your Username">
                                        
                                          </div>
                                    </div>
                                </div> 
                            </div>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="password">Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="password" name="password" class="form-control" placeholder="Enter Your Password"autocomplete="off">
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <label for="password">Confirm Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="c_password" name="c_password" class="form-control" placeholder="Enter Password Again"autocomplete="off">
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="company_id">Company ID </label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input type="number" id="company_id" name="company_id" class="form-control" placeholder="Enter Company ID"autocomplete="off" min="0">                      
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 mt-2">
                              
                                    <label for="permissions">Permission</label>
                                    <div class="form-group">
                                    <!-- <label for="exampleFormControlSelect1">Example select</label> -->
    <select class="form-control select2 select2-choices" name="permissions[]"id="permissions"multiple="multiple">
      <option value="create_orders">Create Orders</option>
      <option Value="read_rma"> Create RMA</option>
    </select>
                                    </div>
                                </div>                              
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Profile Picture</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            
                                            <input id="uploadImage" type="file" name="profile_picture"  class="form-control hidden" onchange="PreviewImage();" />
                                            <label for="uploadImage" class="btn btn-secondary">Click Me To Change Image</label>                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <img src="{{ url('/') }}/common_path_for_all_image/no_image.jpg" id="uploadPreview" style="width: 100px; height: 70px;" />
                                      </div>
                                </div>    
                            </div>
                            
                          
                          <div class="row">
                          <div class="col-md-6">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-primary waves-effect">Create</button>
                                    </div>
                                </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };

</script>

<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
    $(document).ready(function() {
    $('.select2').select2({
    placeholder: "Select permission",
    allowClear: true
});
});
</script> 

<script type="text/javascript">
      $( "#myform" ).validate({
          rules: {
            first_name: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            last_name: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            email: {
              required: true,
              email: true
            },
            username: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            m_number: {
              required: true,
              minlength: 10,
              maxlength: 10
            },
            profile_picture: {
              required: true
            },
            role: {
              required: true
            },
            password: {
                required: true,
                minlength: 8,
                maxlength: 16
            },
            c_password: {
                equalTo: "#password"
            },
            
          },
          messages: {
            first_name: {
              required: "Please provide a first name",
            },
            last_name: {
              required: "Please provide a last name",
            },
            email: {
              required: "Please provide a email",
              email: "Please provide a valid email"
            },
            username: {
              required: "Please provide a username",
            },
            m_number: {
              required: "Please provide a mobile number",
            },
            profile_picture: {
              required: "Please provide a profile picture",
            },
            role: {
              required: "Please provide a role",
            },
            password: {
              required: "Please provide a password",
              minlength: "Your password must be at least 8 characters long"
            },
            c_password: {
              equalTo: "Your password must be at same",
            },
            
          }
        });
    </script>
    
@endsection     
