@extends('admin.layout')

@section('content')
<?php
$i = 1;
?>
<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Orders Report
            </h2>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                <li class="breadcrumb-item active">Orders Report</li>
            </ul>
        </div>
    </div>
</div>
<!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
               @if ($message = Session::get('success'))
               <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                  <span class="font-weight-semibold">Well done!</span> {{$message}}.
              </div>
              @endif 
              <div class="header">
                <ul class="header-dropdown m-r--5">
                    <a class="btn btn-raised btn-primary m-t-15 waves-effect" href="{{route('order.export')}}">Export</a>
                </ul><br>
            </div>            

            <div class="body table-responsive">
                <table class="table table-bordered table-hover js-basic-example " id="order_report_table">
                    <thead style="background-color:#ededed;">
                        <tr class="text-center">
                            <th>ID</th>
                           <!-- <th>Company ID</th>-->
                            <th>Partner ID</th>
                            <th>Offer ID</th>
                            <th>Contact ID</th>
                            <th>Delivery Address Name</th>
                            <th>Delivery Address Street</th>
                            <th>Delivery Address City</th>
                            <th>Delivery Address Zip</th>
                            <th>Delivery Address Province</th>
                            <th>Delivery Address Country</th>
                            <th>Billing Address Name</th>
                            <th>Billing Address Street</th>
                            <th>Billing Address City</th>
                            <th>Billing Address Zip</th>
                            <th>Billing Address Province</th>
                            <th>Billing Address Country</th>
                            <th>Customer Contact</th>
                            <th>order_date</th>
                            <th>valid_upon</th>
                            <th>edi</th>
                            <th>vid_name</th>
                            <th>vid_link</th>
                            <th>vad_company</th>
                            <th>vad_name</th>
                            <th>price</th>
                            <th>status</th>
                            <th>discount</th>          
                        </tr>
                    </thead>

                    <tbody>
                      
                 </tbody>

             </table>
         </div>
     </div>
 </div>
</div>
</div>
</div>
</div>
</div> 
</div>  

<script type="text/javascript">
   $(function () {
             var table = $('#order_report_table').DataTable({
                   processing: true,
                   serverSide: true,
                   responsive: true,
                   ajax: "{{ route('reports.orders') }}",
                   columns: [
                       {data: 'id', name: 'id'},
                   /*    {data: 'company_id', name: 'company_id'},*/
                       {data: 'partner_id', name: 'partner_id'},
                       {data: 'offer_id', name: 'offer_id'},
                       {data: 'contact_id', name: 'contact_id'},
                       {data: 'deliveryaddress_name', name: 'deliveryaddress_name'},
                       {data: 'deliveryaddress_street', name: 'deliveryaddress_street'},
                       {data: 'deliveryaddress_city', name: 'deliveryaddress_city'},
                       {data: 'deliveryaddress_zip', name: 'deliveryaddress_zip'},
                       {data: 'deliveryaddress_province', name: 'deliveryaddress_province'},
                       {data: 'deliveryaddress_country', name: 'deliveryaddress_country'},
                       {data: 'billingaddress_name', name: 'billingaddress_name'},
                       {data: 'billingaddress_street', name: 'billingaddress_street'},
                       {data: 'billingaddress_city', name: 'billingaddress_city'},
                       {data: 'billingaddress_zip', name: 'billingaddress_zip'},
                       {data: 'billingaddress_province', name: 'billingaddress_province'},
                       {data: 'billingaddress_country', name: 'billingaddress_country'},
                       {data: 'customer_contact', name: 'customer_contact'},
                       {data: 'order_date', name: 'order_date'},
                       {data: 'valid_upon', name: 'valid_upon'},
                       {data: 'edi', name: 'edi'},
                       {data: 'vid_name', name: 'vid_name'},
                       {data: 'vid_link', name: 'vid_link'},
                       {data: 'vad_company', name: 'vad_company'},
                       {data: 'vad_name', name: 'vad_name'},
                       {data: 'price', name: 'price'},
                       {data: 'status', name: 'status'},
                       {data: 'discount', name: 'discount'},
                   ]
               });

             
        });
</script>



@endsection     