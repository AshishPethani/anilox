@extends('admin.layout')

@section('content')
<?php
$i = 1;
?>
<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>Product Report</h2>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                <li class="breadcrumb-item active">Product Report</li>
            </ul>
        </div>
    </div>
</div>
<!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
              @if ($message = Session::get('success'))
              <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                  <span class="font-weight-semibold">Well done!</span> {{$message}}.
              </div>
              @endif 
              <div class="header">
                <ul class="header-dropdown m-r--5">
                    <a class="btn btn-raised btn-primary m-t-15 waves-effect" href="{{route('product.export')}}">Export</a>
                </ul><br>
            </div>               

            
            <div class="body table-responsive">
                <table class="table table-bordered table-hover js-basic-example " id="product_report_table">
                    <thead style="background-color:#ededed;">
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Name</th>
                            <th>Article</th>
                            <th>Volume</th>
                            <th>Engraving Type</th>
                            <th>Line Screen</th>
                            <th>Gravur</th>
                            <th>Raster</th>
                            <th>Volumen</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        
                </tbody>
                
            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div> 
</div>  

<div class="modal" id="DeleteArticleModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <h5 class="modal-title">User Delete</h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <!-- Modal body -->
            <div class="modal-body">
                <h7>Are you sure want to delete this User?</h7>
            </div>
            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="SubmitDeleteArticleForm">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(function () {
        var table = $('#product_report_table').DataTable({
                   processing: true,
                   serverSide: true,
                   responsive: true,
                   ajax: "{{ route('reports.products') }}",
                   columns: [
                       {data: 'id', name: 'id'},
                       {data: 'product', name: 'product'},
                       {data: 'article', name: 'article'},
                       {data: 'volume', name: 'volume'},
                       {data: 'engraving_type', name: 'engraving_type'},
                       {data: 'linescreen', name: 'linescreen'},
                       {data: 'gravur', name: 'gravur'},
                       {data: 'raster', name: 'raster'},
                       {data: 'volumen', name: 'volumen'},
                   ]
               });
        });
    </script>

@endsection     