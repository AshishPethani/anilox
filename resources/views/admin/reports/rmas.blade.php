@extends('admin.layout')

@section('content')
<?php
$i = 1;
?>
<div class="block-header">
    <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12">
            <h2>RMA Requests Report</h2>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12">
            <ul class="breadcrumb float-md-right">
                <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                <li class="breadcrumb-item active">ORMA Requests Report</li>
            </ul>
        </div>
    </div>
</div>
<!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
<div class="container-fluid">
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12">
            <div class="card">
               @if ($message = Session::get('success'))
               <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                  <span class="font-weight-semibold">Well done!</span> {{$message}}.
              </div>
              @endif
              <div class="header">
               <ul class="header-dropdown m-r--5">
                    <a class="btn btn-raised btn-primary m-t-15 waves-effect" href="{{route('rma.export')}}">Export</a>
                </ul><br>
            </div>
            
            
            <div class="body table-responsive">
                <table class="table table-bordered table-hover js-basic-example " id="rmas_report_table">
                    <thead style="background-color:#ededed;">
                        <tr class="text-center">
                            <th>ID</th>
                            <th>Customer Address</th>
                            <th>Order Number</th>
                            <th>Serial Number</th>
                            <th>Report Date</th>
                            <th>Customer Ref-number</th>  
                        </tr>
                    </thead>
                    
                    <tbody>
                      
                </tbody>
                
            </table>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div> 
</div>  
    <script type="text/javascript">
        $(function () {
             var table = $('#rmas_report_table').DataTable({
                   processing: true,
                   serverSide: true,
                   responsive: true,
                   ajax: "{{ route('reports.rmas') }}",
                   columns: [
                       {data: 'id', name: 'id'},
                       {data: 'customer_address', name: 'customer_address'},
                       {data: 'order_number', name: 'order_number'},
                       {data: 'serial_number', name: 'serial_number'},
                       {data: 'report_date', name: 'report_date'},
                       {data: 'customer_refnumber', name: 'customer_refnumber'},
                   ]
               });

             
        });
    </script>

@endsection     