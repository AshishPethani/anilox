@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View RMA</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">View RMA</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible status-alert" id="status_alert">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold" id="status_val"></span>
                        </div>
                    <ul class="header-dropdown m-r--5">
                            <!-- <a href="{{route("rmas.edit",$rma->id)}}"><i class="zmdi zmdi-edit"></i></a> -->
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            <input type = "hidden" name = "_token" value = '<?php echo csrf_token(); ?>'>
                           <input type ="hidden" name="id" id="id" value="{{$rma->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Customer Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="customer_address" name="customer_address" class="form-control" value="{{$rma->customer_address}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Order Number</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="order_number" name="order_number" class="form-control" value="{{$rma->order_number}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                  
                            </div>
                            
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">Serial Number</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="serial_number" name="serial_number" class="form-control" value="{{$rma->serial_number}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Report Date</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="report_date" name="report_date" class="form-control" value="{{$reportDate}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                 <div class="col-md-6">
                                    <label for="email_address">RMA Status</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            
                                           <select class="form-control select2" name="rma_status" id="rma_status">
                                            <option value="">--Select--</option>
                                            <option {{ ($rma->rma_status) == 'New' ? 'selected' : '' }}  value="New">New</option>
                                            <option {{ ($rma->rma_status) == 'In Progress' ? 'selected' : '' }}  value="In Progress">In Progress</option>
                                        </select>
                                        </div>
                                    </div>
                                    
                                    
                            </div> 
                                <div class="col-md-6">
                                    <label for="email_address">Customer Ref-number</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="customer_refnumber" name="customer_refnumber" class="form-control" value="{{$rma->customer_refnumber}}" readonly="">
                                        </div>
                                    </div>
    
                            </div> 
                          
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Error Type</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea  type="text"style="height: 255px;" cols="80" rows="12" id="error_type" name="error_type" class="form-control" value="{{$rma->error_type}}" readonly="">{{$rma->error_type}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-md-6">
                                    <label for="email_address">Detail</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea  style="height: 255px;" cols="80" rows="12" type="text" id="description" name="description"class="form-control" value="{{$rma->description}}" readonly="">{{$rma->description}}</textarea>
                                        </div>
                                    </div>
                                </div>  
                       
                            </div>
                        </form>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2({
            allowClear: true
        });
        $("#status_alert").hide();
        
        $("#rma_status").on('change',function(){
           var status = this.value;
           var id = $("#id").val();
           var formData = {id:id,status:status,_token:"{{csrf_token()}}"};
        
            $.ajax({
                url : "{{ url('rmas/edit_status') }}",
                type: "POST",
                data : formData,
                success: function(data)
                {
                    console.log(data['status']);
                    if(data['status'] == 200)
                    {
                        $("#status_alert").show();
                        $("#status_val").html(data['message']);
                        $('#status_alert').delay(5000).fadeOut('slow');
                    }
                },
                error: function ()
                {
                    console.log('error');
                }
            });
        });
    });
</script>
@endsection     