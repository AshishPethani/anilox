@extends('admin.layout')

@section('content')
<?php
$i = 1;
?>
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>RMA List</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">RMA List</li>
                </ul>
            </div>
        </div>
    </div>
   <!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                     @if ($message = Session::get('success'))
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold">Well done!</span> {{$message}}.
                              </div>
                      @endif
                    <div class="header">
                        <ul class="header-dropdown">
                            <!-- <a href="{{route('users.create')}}" class="btn btn-raised btn-primary m-t-15 waves-effect">Add Product</a> -->
                        </ul>
                    </div>
                
                    
                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover js-basic-example " id="rmas_table">
                            <thead style="background-color:#ededed;">
                                <tr class="text-center">
                                <th>ID</th>
            <th>Customer Address</th>
            <th>Order Number</th>
            <th>Serial Number</th>
            <th>Report Date</th>
            <th>Customer Ref-number</th>
            <th>Action</th>
                                </tr>
                            </thead>
                            
                           
                        </table>
                    </div>
                </div>
            </div>
        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>  

    <div class="modal" id="DeleteArticleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">User Delete</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h7>Are you sure want to delete this User?</h7>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeleteArticleForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
             var table = $('#rmas_table').DataTable({
                   processing: true,
                   serverSide: true,
                   responsive: true,
                   ajax: "{{ route('rmas.index') }}",
                   columns: [
                       {data: 'id', name: 'id'},
                       {data: 'customer_address', name: 'customer_address'},
                       {data: 'order_number', name: 'order_number'},
                       {data: 'serial_number', name: 'serial_number'},
                       {data: 'report_date', name: 'report_date'},
                       {data: 'customer_refnumber', name: 'customer_refnumber'},
                       {data: 'action', name: 'action', orderable: false, searchable: false},
                   ]
               });

                var deleteID;
                $('body').on('click', '#getDeleteId', function () {
                    deleteID = $(this).data('id');
                })
                $('#SubmitDeleteArticleForm').click(function (e) {
                    e.preventDefault();
                    var id = deleteID;
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "delete_user/" + id,
                        method: 'GET',
                        success: function (result) {
                            // $('#DeleteArticleModal').hide();
                            // $('#DeleteArticleModal').modal('hide');
                            
                            window.location.href = "delete_success_message_user";
                            location.reload();
                            // $(".alert").hide();
                            // $(".notify_alert").click();
                        }
                    });
                });


        });
    </script>
    
@endsection     