@extends('admin.layout')

@section('content')
    <div class="block-header">
    <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Edit Announcement</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Edit Announcement</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="{{ route('updatedata',[$data->id]) }}" id="myform" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="name">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title" name="title" class="form-control" value="{{ $data->title }} " placeholder="Enter Title" >
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label for="name">Description</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea id="description" name="description" class="form-control" placeholder="Enter Description" row="3">{{ $data->description }} </textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label for="name">URL</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="url" name="url" class="form-control" value="{{ $data->url }} " placeholder="Enter URL">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="Image">Picture</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <input id="uploadImage" type="file" name="image"  class="form-control hidden" onchange="PreviewImage();" accept=".png, .jpg, .jpeg" />
                                            <label for="uploadImage" class="btn btn-secondary">Click Me To Update Image</label>       
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                      <div class="form-group">
                                       <img src='{{ asset($data->image) }}' id="uploadPreview" height='100' />
                                      </div>
                                </div>                                            
                            </div>
                            
                          
                          <div class="row">
                          <div class="col-md-6">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-primary waves-effect">Update</button>
                                    </div>
                                </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };


    $( "#myform" ).validate({
          rules: {
            title: {
              required: true,
              minlength: 2,
              maxlength: 20
            },
            description: {
              required: true,
              minlength: 10,
              maxlength: 500
            },
            url: {
              required: true,
            },
            
          },
          messages: {
            title: {
              required: "Please Enter a Title",
            },
            description: {
              required: "Please Enter a Description",
            },
            url: {
              required: "Please Enter a URL",
            },
            
          }
        });
    </script>
    
@endsection     
