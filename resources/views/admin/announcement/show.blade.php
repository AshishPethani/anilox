@extends('admin.layout')

@section('content')
    <div class="block-header">
    <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Show Announcement</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Show Announcement</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your lable.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="PUT" enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="name">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <lable id="title" name="title" class="form-control" placeholder="Enter Title" >{{ $data->title }}</lable>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label for="name">Description</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <lable>{{ $data->description }} </lable>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <label for="name">URL</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <lable id="url" name="url" class="form-control" value=" " placeholder="Enter URL">{{ $data->url }}</lable>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <label for="Image">Picture</label>
                                    
                                      <div class="form-group">
                                          <img src='{{ asset($data->image) }}' height='100' />
                                      </div>
                                </div>    
                            </div>
                                               
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
 
@endsection     
