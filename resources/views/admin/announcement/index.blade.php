@extends('admin.layout')

@section('content')
    <?php
    $i = 1;
    ?>
    
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Announcements List</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">Announcements List</li>
                </ul>
            </div>
        </div>
    </div>
    <!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                        <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <span class="font-weight-semibold">Well done!</span> {{$message}}.
                        </div>
                    @endif

                    <div class="header">
                        <ul class="header-dropdown m-r--5">
                            <a href="{{route('announcement.create')}}" class="btn btn-raised btn-primary m-t-15 waves-effect">Add Announcement</a>
                        </ul><br>
                    </div>
                
                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover js-basic-example " id="announcement_table">
                        <thead style="background-color:#ededed;">
                            <tr class="text-center">
                                <th>ID</th>
                                <th style="">Image</th>
                                <th>URL</th>
                                <th>Title</th>
                                <th style="width: 300px;">Description</th>
                                <th>Created-At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="https://unpkg.com/sweetalert2@7.8.2/dist/sweetalert2.all.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script type="text/javascript">
        $(function () {
            setTimeout(function(){
                $(".alert").remove();
            }, 3000 ); 
   
            var table = $('#announcement_table').DataTable({
                responsive: true,
                order:[0,'desc'],
                ajax: "{{ route('announcement.index') }}",
                columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: true, searchable: true },
                    {data: 'image', name: 'image', orderable: true, searchable: true},
                    {data: 'url', name: 'url', orderable: false, searchable: false},
                    {data: 'title', name: 'title', orderable: true, searchable: true},
                    {data: 'description', name: 'description', orderable: true, searchable: true},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action'},
                ]
            });


        $(document).on("click", ".remove", function (e) {
            var id = $(this).data("id");
            var token = $(this).data("token");
            swal.fire({
                title: "Delete Announcement",
                text: "Are You Sure Want to Delete this Announcement?",
                type: "warning",
                showCancelButton: !0,
                confirmButtonText: "Delete!",
                cancelButtonText: "Cancel",
                reverseButtons: !0
            }).then(function (e) {

                if (e.value === true) {
                    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                    $.ajax(
                        {
                            url: "{{ url('delete_announcement')}}/"+id,
                            type: 'GET',
                            data: {
                                "id": id,
                                "_token": token,
                            },
                            success: function (response) {
                                if (response.success == true) {
                                    swal.fire("Announcement not delete!!", response.message, "error");
                                    // location.reload();
                                } else {
                                    swal.fire("Announcement Deleted Successfully!", response.message, "success");
                                    location.reload();
                                }
                            }
                        });
                } else {
                    e.dismiss;
                }

            })

        });

           
        });
    </script>
@endsection
