@extends('admin.layout')

@section('content')
                    
    <div class="card shadow" style="margin-top:70px;">
        <div class="card-header py-3">                    
            <h6 class="m-0 font-weight-bold text-primary">
            <a href="{{route('go_back')}}" class="float-right"><i class="zmdi zmdi-arrow-left"></i></a>
            Show Video</h6>
        </div>
        <div class="card-body">            
                <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="name">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <lable><b> {{ $data->title }} </b></lable>
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="row">
                        {{--Video--}}
                        <div class="form-group col-md-12">
                                   <video src=' {{ asset($data->video) }} ' style=" height: 400px;" controls> </video>
                        </div>
                               
                    </div>
                   
                </div>
       
        </div>
    </div>
@endsection

