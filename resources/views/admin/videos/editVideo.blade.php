@extends('admin.layout')

@section('content')
                    
    <div class="card shadow" style="margin-top:70px;">
        <div class="card-header py-3">
                   
                    
            <h6 class="m-0 font-weight-bold text-primary ">
            <a href="{{route('go_back')}}" class="float-right"><i class="zmdi zmdi-arrow-left"></i></a>
             Update Video</h6>
        </div>
        <div class="card-body">
            <form id="myform" method="POST" action="{{route('video.update', $data->id) }}" enctype="multipart/form-data">
                @csrf
                <div class="container">
                            <div class="row">
                                <div class="col-sm-12">
                                    <label for="name">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title" name="title" class="form-control" placeholder="Enter Title" value="{{ $data->title }}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                    <div class="row">
                        {{--Video--}}
                        <div class="form-group col-md-12">
                            <label for="video">Update Video</label>
                            <input type="file" class="form-control @error('video') is-invalid @enderror"
                                   id="video" placeholder="Video" name="video">
                            @error('video')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-12">
                            <div class="progress" 
                            style="position:relative; width:100%;height:1rem !important;font-weight: bold !important;">
                            <div class="bar" style=" background-color: #5195ee; width:0%; height:20px;"></div >
                            <div class="percent" style=" position:absolute; display:inline-block; left:50%; color: #040608;">0%</div >
                        </div>

                        <div class="form-group col-md-12" >
                            <video src=' {{ asset($data->video) }} ' style="width: 200px; height: 150px; margin-top:15px;" controls> </video>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-12">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
    <script src="https://unpkg.com/sweetalert2@7.8.2/dist/sweetalert2.all.js"></script>
    <script>

        $("#myform").validate({
            rules: {
                title: {
                    required: true,
                    minlength: 2,
                    maxlength: 20
                }
              
            },
            messages: {
                title: {
                    required: "Title is required",
                },
                video: {
                    required: "Video is required",
                    extension: "Video must be a type of mp4, mov, ogg,qt, 3gp"
                }
            }
        });
        
        var SITEURL = "{{URL('/')}}";
        $(function() {
            $(document).ready(function()
            {
                var bar = $('.bar');
                var percent = $('.percent');
                $('#myform').ajaxForm({
                    beforeSend: function() {
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function(event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    complete: function(xhr) {
                      return window.location.href = SITEURL +"/video/index";
                    }
                });
            });
        });
    </script>
@endsection