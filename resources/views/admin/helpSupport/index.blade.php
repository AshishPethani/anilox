@extends('admin.layout')

@section('content')

<div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Help & Support List
                <small class="text-muted">Welcome to Anilox Rollers Application</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Help & Support List List</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
    @if ($message = Session::get('success'))

        <div class="alert alert-success">

            <p>{{ $message }}</p>

        </div>

    @endif
    <div class="header">
                        <ul class="header-dropdown m-r--5">                          
                        </ul><br>
                    </div> 
                    <div class="body table-responsive">
    <table class="table table-bordered table-striped table-hover js-basic-example dataTable" id="helpsupport_table">
        <thead style="background-color:#ededed;">
        <tr class="text-center">

            <th>ID</th>
            <th>Name</th>
            <th>Email Adress</th>
            <th>Subject</th>
            <th>Feedback</th>
            <th>Action</th>
        </tr>
        </thead>

    </table>


    </div>


      </div>
      </div>
      </div>
      </div>
<script type="text/javascript">
    $(function () {
        var table = $('#helpsupport_table').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: "{{ route('helpSupport.index') }}",
            columns: [
                {data: 'id', name: 'id'},
                {data: 'name', name: 'name',class:'text-capitalize'},
                {data: 'email', name: 'email'},
                {data: 'subject', name: 'subject',class:'text-capitalize'},
                {data: 'feedback', name: 'feedback',orderable: false,class:'text-capitalize'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        $(document).on('click','.btn-delete',function(){
            if(!confirm("Are you sure?")) return;

            var rowid = $(this).data('rowid');

            var el = $(this)
            if(!rowid) return;


            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                type: "delete",
                dataType: 'JSON',
                url: "helpSupport/" + rowid,
                data: {
                    'id' : rowid
                },
                success: function (data) {
                    if (data.success) {
                        table.row(el.parents('tr'))
                            .remove()
                            .draw();
                    }
                }
            }); //end ajax
        })


    });
</script>

@endsection     