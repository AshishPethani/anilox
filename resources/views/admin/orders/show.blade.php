@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Order
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">View Order</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <ul class="header-dropdown m-r--5">
                        <!-- <a href="{{route("orders.edit",$dataArray->id)}}"><i class="zmdi zmdi-edit"></i></a> -->
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                        </ul>
                        <br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Order Number</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="company_id" name="company_id" class="form-control"
                                                   value="{{$dataArray->old_order_id}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Order Date</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="order_date" name="order_date" class="form-control"
                                                   value="{{$dataArray->order_date}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Company ID</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="company_id" name="company_id" class="form-control"
                                                   value="{{$dataArray->company_id}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Partner ID</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="partner_id" name="partner_id" class="form-control"
                                                   value="{{$dataArray->partner_id}}" readonly="">
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Offer ID</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="offer_id" name="offer_id" class="form-control"
                                                   value="{{$dataArray->offer_id}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Contact ID</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="contact_id" name="contact_id" class="form-control"
                                                   value="{{$dataArray->contact_id}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Delivery Address Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_name" name="deliveryaddress_name"
                                                   class="form-control" value="{{$dataArray->deliveryaddress_name}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Delivery Address Street</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_street" name="deliveryaddress_street"
                                                   class="form-control" value="{{$dataArray->deliveryaddress_street}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Delivery Address City</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_city" name="deliveryaddress_city"
                                                   class="form-control" value="{{$dataArray->deliveryaddress_city}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Delivery Address_ Zip</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_zip" name="deliveryaddress_zip"
                                                   class="form-control" value="{{$dataArray->deliveryaddress_zip}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Delivery Address Province</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_province"
                                                   name="deliveryaddress_province" class="form-control"
                                                   value="{{$dataArray->deliveryaddress_province}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Delivery Adress Country</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="deliveryaddress_country"
                                                   name="deliveryaddress_country" class="form-control"
                                                   value="{{$dataArray->deliveryaddress_country}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_name" name="billingaddress_name"
                                                   class="form-control" value="{{$dataArray->billingaddress_name}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address Street</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_street" name="billingaddress_street"
                                                   class="form-control" value="{{$dataArray->billingaddress_street}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address City</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_city" name="billingaddress_city"
                                                   class="form-control" value="{{$dataArray->billingaddress_city}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address zip</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_zip" name="billingaddress_zip"
                                                   class="form-control" value="{{$dataArray->billingaddress_zip}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address Province</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_province"
                                                   name="billingaddress_province" class="form-control"
                                                   value="{{$dataArray->billingaddress_province}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Billing Address Country</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="billingaddress_country" name="billingaddress_country"
                                                   class="form-control" value="{{$dataArray->billingaddress_country}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Customer Contact</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="customer_contact" name="customer_contact"
                                                   class="form-control" value="{{$dataArray->customer_contact}}"
                                                   readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Price</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="price" name="price" class="form-control"
                                                   value="{{$dataArray->price}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Vid Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="vid_name" name="vid_name" class="form-control"
                                                   value="{{$dataArray->vid_name}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Vid Link</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="vid_link" name="vid_link" class="form-control"
                                                   value="{{$dataArray->vid_link}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Vad Company</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="vad_company" name="vad_company" class="form-control"
                                                   value="{{$dataArray->vad_company}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Vad Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="vad_name" name="vad_name" class="form-control"
                                                   value="{{$dataArray->vad_name}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">Discount</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="discount" name="discount" class="form-control"
                                                   value="{{$dataArray->discount}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Status</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="status" name="status" class="form-control"
                                                   value="{{$dataArray->status}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            @if(!empty($dataArray->contact))
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <h6>Sales Contact Details</h6>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="old_sales_contact_id">Sales Contact ID</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="old_sales_contact_id" name="old_sales_contact_id"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->old_sales_contact_id}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="partner_id">Partner ID</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="partner_id" name="partner_id"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->partner_id}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="contact_id">Contact ID</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="contact_id" name="contact_id"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->contact_id}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="name">Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="name"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->name}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="company_number">Company Number</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="company_number" name="company_number"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->company_number}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="type">Type</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="type" name="type"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->type}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="geo_lat">Geo Lat</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="geo_lat" name="geo_lat"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->geo_lat}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="geo_long">Geo Long</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="geo_long" name="geo_long"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->geo_long}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact">Sales Contact</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact" name="sales_contact"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_name">Sales Contact Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_name" name="sales_contact_name"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_name}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_street">Sales Contact Street</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_street" name="sales_contact_street"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_street}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_city">Sales Contact City</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_city" name="sales_contact_city"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_city}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_zip">Sales Contact Zip</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_zip" name="sales_contact_zip"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_zip}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_country">Sales Contact Country</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_country"
                                                       name="sales_contact_country"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_country}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_phone">Sales Contact Phone</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_phone" name="sales_contact_phone"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_phone}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_fax">Sales Contact Fax</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_fax" name="sales_contact_fax"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_fax}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_email">Sales Contact Email</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_email" name="sales_contact_email"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_email}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="sales_contact_language">Sales Contact Language</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="sales_contact_language"
                                                       name="sales_contact_language"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->sales_contact_language}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="notes">Notes</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="notes" name="notes"
                                                       class="form-control"
                                                       value="{{$dataArray->contact->notes}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(!empty($dataArray->position))
                            @foreach($dataArray->position as $val)
                                <div class="row">
                                    <div class="col-md-12">
                                        <br>
                                        <br>
                                        <h6>Order Position Details</h6>
                                        <hr>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            @if(str_contains($val->article, 340))
                                                <img src="{{asset('images/CeramicSleeve.png')}}" alt="" style="width: 150px">
                                                @elseif(str_contains($val->article, 370))
                                                <img src="{{asset('images/ChromeRoller.png')}}" alt="" style="width: 150px">
                                                @elseif(str_contains($val->article, 'DZ'))
                                                <img src="{{asset('images/Cylinder.png')}}" alt="" style="width: 150px">
                                                @else
                                                <img src="{{asset('images/CeramicRoller.png')}}" alt="" style="width: 150px">
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="old_order_position_id">Order Position ID</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="old_order_position_id"
                                                       name="old_order_position_id"
                                                       class="form-control"
                                                       value="{{$val->old_order_position_id}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="offer_id">Offer ID</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="offer_id" name="offer_id"
                                                       class="form-control"
                                                       value="{{$val->offer_id}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="position">Position</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="position" name="position"
                                                       class="form-control"
                                                       value="{{$val->position}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="article">Article</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="article" name="article"
                                                       class="form-control"
                                                       value="{{$val->article}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="product_description">Product Description</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="product_description" name="product_description"
                                                       class="form-control"
                                                       value="{{$val->product_description}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="amount">Amount</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="amount" name="amount"
                                                       class="form-control"
                                                       value="{{$val->amount}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="date_of_delivery">Date Of Delivery</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="date_of_delivery" name="date_of_delivery"
                                                       class="form-control"
                                                       value="{{$val->date_of_delivery}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="drawing">Drawing</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="drawing" name="drawing"
                                                       class="form-control"
                                                       value="{{$val->drawing}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="engraving">Engraving</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="engraving" name="engraving"
                                                       class="form-control"
                                                       value="{{$val->engraving}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="linescreen">Linescreen</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="linescreen" name="linescreen"
                                                       class="form-control"
                                                       value="{{$val->linescreen}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="volume">Volume</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="volume" name="volume"
                                                       class="form-control"
                                                       value="{{$val->volume}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="angle">Angle</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="angle" name="angle"
                                                       class="form-control"
                                                       value="{{$val->angle}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="serial_number">Serial Number</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="serial_number" name="serial_number"
                                                       class="form-control"
                                                       value="{{$val->serial_number}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="customer_serial_number">Customer Serial Number</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="customer_serial_number"
                                                       name="customer_serial_number"
                                                       class="form-control"
                                                       value="{{$val->customer_serial_number}}"
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="status">Status</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="status" name="status"
                                                       class="form-control"
                                                       value="{{$val->status}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @endif
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
