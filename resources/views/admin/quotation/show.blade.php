@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Quotation</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">View Quotation</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                          <ul class="header-dropdown m-r--5">
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                          </ul><br>
                        <div class="alert bg-success text-white alert-styled-left alert-dismissible status-alert"
                             id="status_alert">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <span class="font-weight-semibold" id="status_val"></span>
                        </div>
                        <br>
                    </div>
                    @if(empty($data->product))
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            <input type="hidden" name="_token" value='<?php echo csrf_token(); ?>'>
                            <input type="hidden" name="id" id="id" value="{{$data->id}}">

                            <div class="row">

                                <div class="col-md-6">
                                    <label for="name">Customer</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="customer_address" name="customer_address"
                                                   class="form-control" value="{{$data->email}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="machine_manufacture">Machine Manufacture</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="machine_manufacture" name="machine_manufacture"
                                                   class="form-control" value="{{$data->machineManufacture->title}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="machine_type">Machine Type</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="machine_type" name="machine_type"
                                                   class="form-control" value="{{$data->machine_type}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="anilox_type">Anilox Type</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="anilox_type" name="anilox_type" class="form-control"
                                                   value="{{$data->aniloxType->title}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="anilox_surface">Anilox Surface</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="anilox_surface" name="anilox_surface" class="form-control"
                                                   value="{{$data->aniloxSurface->title}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="order_type">Order Type</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="order_type" name="order_type" class="form-control"
                                                   value="{{$data->orderType->title}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="ink_system">Ink System</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="ink_system" name="ink_system" class="form-control"
                                                   value="{{$data->ink_system}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                 <div class="col-md-6">
                                    <label for="ink_system">Cleaning Products Information</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="cms" name="cms" class="form-control"
                                                   value="{{($data->cms == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                  <div class="col-md-6">
                                    <label for="ink_system">Information About Cleaning Machines</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="clean_machine" name="clean_machine" class="form-control"
                                                   value="{{($data->clean_machine == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="ink_system">More Information About Sleeves/Rollers</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="info_sleeves" name="info_sleeves" class="form-control"
                                                   value="{{($data->info_sleeves == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="created_at">Created At</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="created_at" name="created_at" class="form-control"
                                                   value="{{\Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i:s')}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    @else
                        <div class="body">
                            <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="name">Customer</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="name" class="form-control"
                                                       value="{{$data->email}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                    <label for="ink_system">Cleaning Products Information</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="cms" name="cms" class="form-control"
                                                   value="{{($data->cms == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <label for="ink_system">Information About Cleaning Machines</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="clean_machine" name="clean_machine" class="form-control"
                                                   value="{{($data->clean_machine == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="ink_system">More Information About Sleeves/Rollers</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="info_sleeves" name="info_sleeves" class="form-control"
                                                   value="{{($data->info_sleeves == '0') ? 'No' : 'Yes'}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                </div>

                                <div class="row">

                                    <div class="col-md-6">
                                        <label for="name">Product Name</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="name" name="name" class="form-control"
                                                       value="{{$data->product->product}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Raster</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="raster" name="raster" class="form-control"
                                                       value="{{$data->product->raster}}" readonly="">
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="email_address">Volumen</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="volumen" name="volumen" class="form-control" value="{{$data->product->volumen}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Gravur</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="gravur" name="gravur" class="form-control" value="{{$data->product->gravur}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="email_address">Volume</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="volume" name="volume" class="form-control" value="{{$data->product->volume}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Angle</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="angle" name="angle" class="form-control" value="{{$data->product->angle}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="email_address">Engraving Type</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="engraving_type" name="engraving_type" class="form-control" value="{{$data->product->engraving_type}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Line-Screen</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <input type="text" id="linescreen" name="linescreen" class="form-control" value="{{$data->product->linescreen}}" readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <label for="email_address">Article</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea type="text" id="article" name="article" class="form-control" value="{{$data->product->article}}" readonly="">{{$data->product->article}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="email_address">Detail</label>
                                        <div class="form-group">
                                            <div class="form-line">
                                                <textarea type="text" id="description" name="description" class="form-control" value="{{$data->product->description}}" readonly="">{{$data->product->description}}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#status_alert").hide();
            });
        </script>
@endsection
