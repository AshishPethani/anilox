@extends('admin.layout')

@section('content')
    <?php
    $i = 1;
    ?>
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Quotation List</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">Quotation List</li>
                </ul>
            </div>
        </div>
    </div>
    <!--  <button class="btn btn-raised btn-primary waves-effect" data-type="autoclose-timer">CLICK ME</button> -->
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                        <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <span class="font-weight-semibold">Well done!</span> {{$message}}.
                        </div>
                    @endif

                    <div class="body table-responsive">
                        <table class="table table-bordered table-hover js-basic-example " id="quotation_table">
                            <thead style="background-color:#ededed;">
                            <tr class="text-center">
                                <th>ID</th>
                                <th>Customer</th>
                                <th>Article</th>
                                <th>Machine Manufacture</th>
                                <th>Machine Type</th>
                                <th>Anilox Type</th>
                                <th>Anilox Surface</th>
                                <th>Order Type</th>
                                <th>Ink System</th>
                                <th>Cleaning Products Information</th>
                                 <th>Information About Cleaning Machines</th>
                                <th>More Information About Sleeves/Rollers</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="DeleteArticleModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h5 class="modal-title">Quotation Delete</h5>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <h7>Are you sure want to delete this Quotation?</h7>
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" id="SubmitDeleteArticleForm">Yes</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(function () {
            var table = $('#quotation_table').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                order:[0,'desc'],
                ajax: "{{ route('quotation.index') }}",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'email', name: 'email', orderable: true,  searchable: true},
                    {data: 'article', name: 'article', orderable: true},
                    {data: 'machine_manufacture', name: 'machine_manufacture'},
                    {data: 'machine_type', name: 'machine_type'},
                    {data: 'anilox_type', name: 'anilox_type'},
                    {data: 'anilox_surface', name: 'anilox_surface'},
                    {data: 'order_type', name: 'order_type'},
                    {data: 'ink_system', name: 'ink_system'},
                    {data:'cms', name:'cms'},
                    {data:'clean_machine', name:'clean_machine'},
                    {data:'info_sleeves', name:'info_sleeves'},
                    {data: 'created_at', name: 'created_at'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            var deleteID;
            $('body').on('click', '#getDeleteId', function () {
                deleteID = $(this).data('id');
            })
            $('#SubmitDeleteArticleForm').click(function (e) {
                e.preventDefault();
                var id = deleteID;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "destroy/" + id,
                    method: 'GET',
                    success: function (result) {
                        window.location.href = "delete_success_message_user";
                        location.reload();
                    }
                });
            });
        });
    </script>
@endsection
