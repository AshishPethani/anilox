@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Send Notification</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">Send Notification</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        
                        @if ($message = Session::get('success'))
                            <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <span class="font-weight-semibold">Well done!</span> {{$message}}.
                        </div>
                        @endif 
                        <ul class="header-dropdown m-r--5">
                            <a href="{{url('dashboard')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                        </ul>
                        <br>
                    </div>
                    <div class="body">
                        <form action="{{ route('notification.storeNotification') }}" id="myform" method="POST" enctype="multipart/form-data"
                              runat="server">
                            @csrf
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="title">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title" name="title" placeholder="title" class="form-control">
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="description">Description</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description" name="description" placeholder="description" class="form-control"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-raised btn-primary waves-effect">Send Notification</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>

    <script type="text/javascript">
            setTimeout(function(){
                $(".alert").remove();
            }, 4000 ); 


        $("#myform").validate({
            rules: {
                title: {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                description: {
                    required: true,
                     minlength: 10,
                    maxlength: 350
                },
            },
            messages: {
                title: {
                    required: "Please provide a title",
                },
                description: {
                    required: "Please provide a description",
                },
            }
        });

    </script>
@endsection     
