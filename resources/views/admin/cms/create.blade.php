@extends('admin.layout')

@section('content')
    <div class="block-header">
    <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>New CMS Page</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Add CMS Page</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <a href="{{url('cms')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="{{ route('cms.store') }}" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            @csrf
                            <div class="row">
                             <div class="col-md-12">
                                    <label for="email_address">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                             <input type="text" id="title" name="title" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                
                            </div> 
                            <div class="row">
                          	<div class="col-md-12">
                                    <label for="content">Content</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="content" name="content" class="ckeditor form-control"></textarea>
                                        </div>
                                    </div>
                                </div>  
                            </div>    
                          <div class="row">
                          <div class="col-md-6">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-primary waves-effect">Create</button>
                                    </div>
                                </div>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };

</script>
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
    $(document).ready(function() {
    $('.select2').select2({
    placeholder: "Select permission",
    allowClear: true
});
});
</script> 

<script type="text/javascript">

    $( "#myform" ).validate({
        rules: {
            title: {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            content: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please provide a title",
            },
            content: {
                required: "Please provide a content",
            },
        }
    });
    
    $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
</script>
@endsection     
