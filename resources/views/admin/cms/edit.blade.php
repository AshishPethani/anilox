@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Edit CMS Page</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Edit CMS Page</li>
                </ul>
            </div>
        </div>
    </div>
 
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                     @if ($message = Session::get('success'))
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold">Well done!</span> {{$message}}.
                              </div>
                      @endif
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown">
                            
                            <a href="{{url('cms')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="{{ route('cms.update',$data->id) }}" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                            @csrf
                           @method('PUT')
                           <div class="row">
                            <div class="col-md-12">
                                    <label for="email_address">Title</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                             <input type="text" id="title" name="title" class="form-control" value="{{$data->title}}">
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                            <div class="row">	
                            	<div class="col-md-12">
                                    <label for="content">Content</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="content" name="content" class="ckeditor form-control" value="{{$data->content}}" style="height:300px;">{{$data->content}}</textarea>
                                        </div>
                                    </div>
                                </div> 
			     </div>     
                            <div class="row">
                          
                                <div class="col-md-6 mt-4">
                                    <div class="form-group">
                                    <button type="submit" class="btn btn-raised btn-primary waves-effect">Save</button>
                                      </div>
                                </div> 
                            </div>
                           
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">

    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };

</script>

<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
</script> 
<script src="//cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>

<script type="text/javascript">

    $( "#myform" ).validate({
        rules: {
            title: {
                required: true,
                minlength: 2,
                maxlength: 150
            },
            content: {
                required: true,
            },
        },
        messages: {
            title: {
                required: "Please provide a title",
            },
            content: {
                required: "Please provide a content",
            },
        }
    });
    
     $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
</script>
@endsection     
