@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View FAQs</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">View FAQs</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                           
                            <a href="{{url('faqs')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">

                            <div class="row">
                            	<div class="col-md-12">
                                    <label for="title_english">Title English</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title_english" name="title_english" class="form-control" value="{{$data->title_english}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                   
                            </div> 
                            <div class="row">
                            	<div class="col-md-12">
                                    <label for="description_english">Description English</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description_english" name="description_english"
                                                      class="form-control" value="{{$data->description_english}}"
                                                      readonly="" style="height:200px;
">{{$data->description_english}}</textarea>
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="title_german">Title German</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title_german" name="title_german" class="form-control" value="{{$data->title_german}}" readonly="">
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="description_german">Description German</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description" name="description"
                                                      class="form-control" value="{{$data->description_german}}"
                                                      readonly="" style="height:200px;">{{$data->description_german}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    

@endsection     
