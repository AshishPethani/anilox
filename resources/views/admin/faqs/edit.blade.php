@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Edit FAQs</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a>
                    </li>
                    <li class="breadcrumb-item active">Edit FAQs</li>
                </ul>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    @if ($message = Session::get('success'))
                        <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                            <span class="font-weight-semibold">Well done!</span> {{$message}}.
                        </div>
                    @endif
                    <div class="header">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        <ul class="header-dropdown">

                            <a href="{{url('faqs')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                        </ul>
                        <br>
                    </div>
                    <div class="body">
                        <form action="{{ route('faqs.update',$data->id) }}" id="myform" method="POST"
                              enctype="multipart/form-data" runat="server">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="title_english">Title English</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title_english" name="title_english"
                                                   class="form-control" value="{{$data->title_english}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="description_english">Detail English</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description_english" name="description_english"
                                                      class="form-control" value="{{$data->description_english}}"
                                                      style="height:200px;">{{$data->description_english}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="title_german">Title German</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="title_german" name="title_german"
                                                   class="form-control" value="{{$data->title_german}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="description_german">Detail German</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description_german" name="description_german"
                                                      class="form-control" value="{{$data->description_german}}"
                                                      style="height:200px;">{{$data->description_german}}</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-6 mt-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-raised btn-primary waves-effect">Save
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        function PreviewImage() {
            var oFReader = new FileReader();
            oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

            oFReader.onload = function (oFREvent) {
                document.getElementById("uploadPreview").src = oFREvent.target.result;
            };
        };

    </script>

    <script>
        function onlyNumberKey(evt) {

            // Only ASCII charactar in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    </script>

    <script type="text/javascript">

        $("#myform").validate({
            rules: {
                title_english: {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                description_english: {
                    required: true,
                },
                title_german: {
                    required: true,
                    minlength: 2,
                    maxlength: 150
                },
                description_german: {
                    required: true,
                },
            },
            messages: {
                title_english: {
                    required: "Please provide a title",
                },
                description_english: {
                    required: "Please provide a description",
                },
                title_german: {
                    required: "Please provide a title",
                },
                description_german: {
                    required: "Please provide a description",
                },
            }
        });
    </script>
@endsection     
