@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>View Product</h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">View Product</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            <!-- <a href="{{route("products.edit",$product->id)}}"><i class="zmdi zmdi-edit"></i></a> -->
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                        <form action="" id="myform" method="POST" enctype="multipart/form-data" runat="server">
                           
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Product Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="name" name="name" class="form-control" value="{{$product->product}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Raster</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="raster" name="raster" class="form-control" value="{{$product->raster}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                  
                            </div>
                            
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">Volumen</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="volumen" name="volumen" class="form-control" value="{{$product->volumen}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Gravur</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="gravur" name="gravur" class="form-control" value="{{$product->gravur}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">Volume</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="volume" name="volume" class="form-control" value="{{$product->volume}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Angle</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="angle" name="angle" class="form-control" value="{{$product->angle}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">Engraving Type</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="engraving_type" name="engraving_type" class="form-control" value="{{$product->engraving_type}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Line-Screen</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="linescreen" name="linescreen" class="form-control" value="{{$product->linescreen}}" readonly="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="email_address">Article</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="article" name="article" class="form-control" value="{{$product->article}}" readonly="">{{$product->article}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="email_address">Detail</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="description" name="description" class="form-control" value="{{$product->description}}" readonly="">{{$product->description}}</textarea>
                                        </div>
                                    </div>
                                </div>      
                            </div> 
                            
                            
                        </form>
                    </div>
                </div>
            </div>
        </div> 
    </div>  
    

@endsection     