@extends('admin.layout')

@section('content')
<div class="block-header">
    <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Add Product
                <small class="text-muted">Welcome to Product Application</small>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Add Product</li>
                </ul>
            </div>
        </div>
    </div>

        <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">

                <div class="header">

@if ($errors->any())

<div class="alert alert-danger">

    <strong>Whoops!</strong> There were some problems with your input.<br><br>

    <ul>

        @foreach ($errors->all() as $error)

            <li>{{ $error }}</li>

        @endforeach

    </ul>

</div>

@endif

<ul class="header-dropdown m-r--5">
        <a href="{{ route('products.index') }}"> <i class="zmdi zmdi-arrow-left"></i></a>
        </ul><br>
                    </div>

                    <div class="body">

<form action="{{ route('products.update' ,$product->id, $supplier->s_id) }}" method="POST">

@csrf

@method('PUT')
<div class="col-md-12 p-2">
<h5 class="text-primary">Product Details</h5>
<hr>
</div>
<div class="row">
                                <div class="col-md-6">
                                    <label for="name">Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="p_name" name="p_name" value="{{ $product->p_name }}"  class="form-control" placeholder="Enter Product Full Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name">Price</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" id="p_price" name="p_price" value="{{ $product->p_price }}"  class="form-control" placeholder="Enter Product Price">
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Quantity</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" id="p_quantity" name="p_quantity" value="{{ $product->p_quantity }}"  class="form-control" placeholder="Enter Product Quantity">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name">Weight</label>
                                    <div class="form-group">
                                        <div class="form-line">
 <input type="text" id="p_weight" name="p_weight" value="{{ $product->p_weight }}"  class="form-control"  pattern="[0-9]+([\.][0-9]{0,2})?" placeholder="Enter Product Weight">
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="row">
                                <div class="col-md-9">
                                    <label for="name">Description</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <textarea type="text" id="p_description" name="p_description" class="form-control" placeholder="Enter Product Description">{{$product->p_description}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                 
                            </div>

<div class="col-md-12 p-2">
<h5 class="text-primary">Supplier Details</h5>
<hr>
</div>
<div class="row">
                                <div class="col-md-6">
                                    <label for="name">Name</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="s_name" value="{{ $supplier->s_name }}" name="s_name" class="form-control" placeholder="Enter Your Full Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name">Pincode</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="number" id="s_pincode" value="{{ $supplier->s_pincode }}" name="s_pincode" class="form-control" placeholder="Enter Area Pincode">
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Address</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="s_address" value="{{ $supplier->s_address }}" name="s_address" class="form-control" placeholder="Enter Your Full Adress">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="last_name">Additional Address</label>
                                    <!-- <small>it is optional</small> -->
                                    <div class="form-group">
                                        <div class="form-line">
 <input type="text" id="s_address1" name="s_address1" class="form-control" value="{{ $supplier->s_address1 }}" placeholder="Enter Your Full Adress(it is optional)">
                                        </div>
                                    </div>
                                </div>    
                            </div>
                            <div class="row">
                            <div class="col-md-6">
                                    <label for="password">State</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                        <select name="s_state" id="s_state" class="form-control show-tick select2">
                                        <option value="{{ $supplier->s_state }}">-- Please select --</option>
<option value="Andhra Pradesh">Andhra Pradesh</option>
<option value="Andaman and Nicobar Islands">Andaman and Nicobar Islands</option>
<option value="Arunachal Pradesh">Arunachal Pradesh</option>
<option value="Assam">Assam</option>
<option value="Bihar">Bihar</option>
<option value="Chandigarh">Chandigarh</option>
<option value="Chhattisgarh">Chhattisgarh</option>
<option value="Dadar and Nagar Haveli">Dadar and Nagar Haveli</option>
<option value="Daman and Diu">Daman and Diu</option>
<option value="Delhi">Delhi</option>
<option value="Lakshadweep">Lakshadweep</option>
<option value="Puducherry">Puducherry</option>
<option value="Goa">Goa</option>
<option value="Gujarat">Gujarat</option>
<option value="Haryana">Haryana</option>
<option value="Himachal Pradesh">Himachal Pradesh</option>
<option value="Jammu and Kashmir">Jammu and Kashmir</option>
<option value="Jharkhand">Jharkhand</option>
<option value="Karnataka">Karnataka</option>
<option value="Kerala">Kerala</option>
<option value="Madhya Pradesh">Madhya Pradesh</option>
<option value="Maharashtra">Maharashtra</option>
<option value="Manipur">Manipur</option>
<option value="Meghalaya">Meghalaya</option>
<option value="Mizoram">Mizoram</option>
<option value="Nagaland">Nagaland</option>
<option value="Odisha">Odisha</option>
<option value="Punjab">Punjab</option>
<option value="Rajasthan">Rajasthan</option>
<option value="Sikkim">Sikkim</option>
<option value="Tamil Nadu">Tamil Nadu</option>
<option value="Telangana">Telangana</option>
<option value="Tripura">Tripura</option>
<option value="Uttar Pradesh">Uttar Pradesh</option>
<option value="Uttarakhand">Uttarakhand</option>
<option value="West Bengal">West Bengal</option>
</select>
                                        </div>
                                    </div>
                                </div> 
                                <div class="col-md-6">
                                    <label for="name">City</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" id="s_city" name="s_city" value="{{ $supplier->s_city }}" class="form-control" placeholder="Enter Your City Name">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Contact</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" pattern="[789][0-9]{9}" value="{{ $supplier->s_contact }}" id="s_contact" name="s_contact" class="form-control" placeholder="Enter Your Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="name">Additional Contact </label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" pattern="[789][0-9]{9}" value="{{ $supplier->s_contact1 }}" id="s_contact1" name="s_contact1" class="form-control" placeholder="Enter Your Number(it is optional)">
                                        </div>
                                    </div>
                                </div>
                                  
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="name">WhatsApp Contact</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="text" pattern="[789][0-9]{9}" value="{{ $supplier->s_whatsapp }}" id="s_whatsapp" name="s_whatsapp" class="form-control" placeholder="Enter Your WhatsApp Number">
                                        </div>
                                    </div>
                                </div>                               
                            </div>

<button type="submit" class="btn btn-primary">Update</button>

</form>
</div>
</div>
                            </div>
                            </div>
                            </div>


                                <!-- Select2 CSS --> 
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> 

<!-- jQuery --> <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script> 

<!-- Select2 JS --> 
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>

<script>
 $(document).ready(function(){
  $('.select2').select2();

});

</script>
@endsection     