@component('mail::message')
# @lang('Hello!')

Your Quotation request has been submitted successfully
Below are the details submitted for quotation:

@component('mail::table')
@if(empty($data->article))
|          |          |
| ------------- |:-------------:|
|  __Machine Manufacture__ |   {{$data->machineManufacture->title}}    |
|  __Anilox Type__ |   {{$data->aniloxType->title}}    |
|  __Anilox Surface__ |   {{$data->aniloxSurface->title}}    |
|  __Order Type__ |   {{$data->orderType->title}}    |
|  __Machine Type__ |   {{$data->machine_type}}    |
|  __Ink System__ |   {{$data->ink_system}}    |
|  __Cleaning Products Information__ |   @if($data->cms==0) {{'No'}} @else {{'Yes'}} @endif    |
|  __Information About Cleaning Machines__ |   @if($data->clean_machine==0) {{'No'}} @else {{'Yes'}} @endif    |
|  __More Information About Sleeves/Rollers__ |   @if($data->info_sleeves==0) {{'No'}} @else {{'Yes'}} @endif    |
|  __Created At__ |   {{\Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i:s')}}    |
@else
|          |          |
| ------------- |:-------------:|
|  __Article__ |   {{$data->product->article}}    |
|  __Product Name__ |   {{$data->product->product}}    |
|  __Raster__ |   {{$data->product->raster}}    |
|  __Volumen__ |   {{$data->product->volumen}}    |
|  __Gravur__ |   {{$data->product->gravur}}    |
|  __Volume__ |   {{$data->product->volume}}    |
|  __Angle__ |   {{$data->product->angle}}    |
|  __Engraving Type__ |   {{$data->product->engraving_type}}    |
|  __Line-Screen__ |   {{$data->product->linescreen}}    |
|  __Cleaning Products Information__ |   @if($data->cms==0) {{'No'}} @else {{'Yes'}} @endif   |
|  __Information About Cleaning Machines__ |   @if($data->clean_machine==0) {{'No'}} @else {{'Yes'}} @endif    |
|  __More Information About Sleeves/Rollers__ |   @if($data->info_sleeves==0) {{'No'}} @else {{'Yes'}} @endif    |
|  __Detail__ |   {{$data->product->description}}    |
|  __Created At__ |   {{\Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i:s')}}    |
@endif
@endcomponent

Thank you very much for your quotation request. We'll get in touch with you soon. The Zecher Team
@endcomponent
