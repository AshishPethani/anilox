@component('mail::message')
# @lang('Hello!')

Your RMA request has been submitted successfully
Below are the details submitted for RMA:

@component('mail::table')

|          |          |
| ------------- |:-------------:|
|  __Rma Old Id__ |   {{$data->rma_old_id}}    |
|  __Customer Address__ |   {{$data->customer_address}}    |
|  __Order Number__ |   {{$data->order_number}}    |
|  __Serial Number__ |   {{$data->serial_number}}    |
|  __Report Date__ |   {{$data->report_date}}    |
|  __Description__ |   {{$data->description}}    |
|  __Error Type__ |   {{$data->error_type}}    |
|  __Customer Refnumber__ |   {{$data->customer_refnumber}}    |
|  __RMA Status__ |   {{$data->rma_status}}    |
|  __Created At__ |   {{\Carbon\Carbon::parse($data->created_at)->format('d-m-Y H:i:s')}}    |

@endcomponent

Thank you very much for your RMA request. We'll get in touch with you soon. The Zecher Team
@endcomponent
