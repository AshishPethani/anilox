@component('mail::message')
# @lang('Hello!')

Please use below password to login to system:

<h2>{{$password}}</h2>

Thanks,<br>
{{ config('app.name') }}
@endcomponent
