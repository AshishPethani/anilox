@extends('admin.layout')

@section('content')
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Update Your Password Here !
                <!--<small class="text-muted">Welcome to Profile</small>-->
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="{{route('dashboard')}}"><i class="zmdi zmdi-home"></i> Home</a></li>
                    <li class="breadcrumb-item active">Edit Profile</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="card">
                     @if ($message = Session::get('success'))
                      <div class="alert bg-success text-white alert-styled-left alert-dismissible">
                              <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
                              <span class="font-weight-semibold">Well done!</span> {{$message}}.
                              </div>
                      @endif
                    <div class="header">
                        @if (count($errors) > 0)
                      <div class="alert alert-danger">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                           @foreach ($errors->all() as $error)
                             <li>{{ $error }}</li>
                           @endforeach
                        </ul>
                      </div>
                    @endif
                    <ul class="header-dropdown m-r--5">
                            
                            <a href="{{route('go_back')}}"><i class="zmdi zmdi-arrow-left"></i></a>
                    </ul><br>
                    </div>
                    <div class="body">
                    <form method="POST" action="{{ route('change.password') }}">
                        @csrf 
 
                         <div class="row">
                                <div class="col-md-6">
                                    <label for="name">Current Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="password" name="current_password" class="form-control" placeholder="Enter your Old Password" autocomplete="current-password">
                                        </div>
                                    </div>
                                </div> 
                            </div>
                            
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="email_address">New Password </label>
                                    <!-- /<small class="text-danger">(read only)</small> -->
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="new_password" name="new_password" class="form-control" placeholder="Enter New Passowrd" autocomplete="current-password">
                                        </div>
                                    </div>
                                </div>    
                            </div>

                            <div class="row">                

                                <div class="col-md-6">
                                    <label for="password">New Confirm Password</label>
                                    <div class="form-group">
                                        <div class="form-line">
                                            <input type="password" id="new_confirm_password" name="new_confirm_password" class="form-control" placeholder="Re-Enter Password" autocomplete="current-password">
                                        </div>
                                    </div>
                                </div>   
                            </div>
                            <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect">Update Password</button>
                            </form>
                        </div>
                </div>
            </div>
        </div> 
    </div>  
    
<script type="text/javascript">
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>
<script> 
    function onlyNumberKey(evt) { 
          
        // Only ASCII charactar in that range allowed 
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode 
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57)) 
            return false; 
        return true; 
    } 
</script>     
@endsection     