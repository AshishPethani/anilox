<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>:: Anilox Rollers Login ::</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-orange">
<div class="authentication">
    <div class="card">
        <div class="body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="header slideDown">
                        <!-- <div class="logo"><img src="assets/images/logo.png" alt="Nexa"></div> -->
                        <h1 class="text-white">Anilox Rollers</h1>
                        <!-- <ul class="list-unstyled l-social">
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-facebook-box"></i></a></li>
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-linkedin-box"></i></a></li>                            
                            <li><a href="javascript:void(0);"><i class="zmdi zmdi-twitter"></i></a></li>
                        </ul> -->
                    </div>                        
                </div>
                @guest
                <form class="col-lg-12" id="sign_in" action="{{ route('login') }}" method="POST">
                    @csrf
                    <!-- <h5 class="title">Sign in to your Account</h5> -->
                    <br>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email-Address">
                            <!-- <label class="form-label">Email-Address</label> -->
                                <!-- @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror -->
                        </div>
                        
                    </div>
                    <div class="form-group form-float">
                        <div class="form-line">
                            <input id="password" type="password" class="form-control @error('email') is-invalid @enderror  @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
                            <!-- <label class="form-label">Password</label> -->
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                        </div>
                        
                    </div>
                    <!-- <div>
                        <input type="checkbox"class="filled-in chk-col-cyan" name="remember_me" id="remember_me" value="true" {{ old('remember_me') ? 'checked' : '' }}>
                        <label for="remember_me">Remember Me</label>
                    </div>  -->
                    <div class="col-lg-12">
                    <!-- <a href="index.html" class="btn btn-raised btn-primary waves-effect">SIGN IN</a> -->
                    <button type="submit" class="btn btn-raised btn-primary waves-effect">SIGN IN</button>
                    <!-- <a href="sign-up.html" class="btn btn-raised btn-default waves-effect">SIGN UP</a>  -->                       
                    </div>                       
                </form>

                @if (Route::has('password.request'))
                <div class="col-lg-12 m-t-20">
                    <a class="" href="{{ route('password.request') }}">Forgot Password?</a>
                </div> 
                @endif 

                @else
                <center><h4>You are Login as Admin</h4></center>                  
                <p>Visit <a href="{{route('dashboard')}}">Dashboard</a></p>
                @endguest
            </div>
        </div>
    </div>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>    
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script>
</body>
</html>